# -*- coding: UTF-8 -*-
import numpy as np
#import operator
#from os import listdir
from sklearn import svm

"""
sklearn-训练模型
"""

def img2vector(filename):
	"""
	将32x32的二进制图像转换为1x1024向量。
	Parameters:
		filename - 文件名
	Returns:
		returnVect - 返回的二进制图像的1x1024向量
	"""
	#创建1x1024零向量
	returnVect = np.zeros((1, 1024))
	#打开文件
	fr = open(filename)
	#按行读取
	for i in range(32):
		#读一行数据
		lineStr = fr.readline()
		#每一行的前32个元素依次添加到returnVect中
		for j in range(32):
			returnVect[0, 32*i+j] = int(lineStr[j])
	#返回转换后的1x1024向量
	return returnVect

def loadImages(dirName =  r'C:\Users\Administrator\Desktop\ML\SVM\testDigits'):
	"""
	加载图片
	Parameters:
		dirName - 文件夹的名字
	Returns:
	    trainingMat - 数据矩阵
	    hwLabels - 数据标签
	"""
	from os import listdir
	hwLabels = []
	trainingFileList = listdir(dirName)  #文件夹下的文件列表          
	m = len(trainingFileList)
	trainingMat = np.zeros((m,1024))
	for i in range(m):
		fileNameStr = trainingFileList[i]
		fileStr = fileNameStr.split('.')[0]     
		classNumStr = int(fileStr.split('_')[0])
		if classNumStr == 9: hwLabels.append(-1)
		else: hwLabels.append(1)
		trainingMat[i,:] = img2vector('%s/%s' % (dirName, fileNameStr))
	return trainingMat, hwLabels   

def handwritingClassTest():
    """
	手写数字分类测试
	Parameters:
		无
	Returns:
		无
	"""
    #返回trainingDigits目录下的文件名
    #加载训练集数据
    trainingMat,trainingLabels = loadImages(r'C:\Users\Administrator\Desktop\ML\SVM\trainingDigits')
    testMat,testLabels = loadImages(r'C:\Users\Administrator\Desktop\ML\SVM\testDigits')
    clf = svm.SVC(C=200,kernel='rbf')
    clf.fit(trainingMat,trainingLabels)
    #测试集结果
    classifierResult = clf.predict(testMat)
    mTest = len(testMat)
    errorCount = 0
    for i in range(mTest):
        if classifierResult[i] != testLabels[i]:
            errorCount += 1
    print("测试集个数：%d,\n总错误个数：%d,\n错误率为: %f%%" % (mTest,errorCount, errorCount/mTest * 100))

if __name__ == '__main__':
	handwritingClassTest()