'''
交叉验证整理
'''
import numpy as np
from random import randrange, seed, random

#有放回的重抽样
def cross_validation_split_1(dataset, n_folds):
    """cross_validation_split(将数据集进行抽重抽样 n_folds 份，数据可以重复重复抽取，每一次list的元素是无重复的)

    Args:
        dataset     原始数据集
        n_folds     数据集dataset分成n_flods份
    Returns:
        dataset_split    list集合，存放的是：将数据集进行抽重抽样 n_folds 份，数据可以重复重复抽取，每一次list的元素是无重复的
    """
    dataset_split = list()
    dataset_copy = list(dataset)       # 复制一份 dataset,防止 dataset 的内容改变
    fold_size = len(dataset) / n_folds
    for i in range(n_folds):
        fold = list()                  # 每次循环 fold 清零，防止重复导入 dataset_split
        while len(fold) < fold_size:   # 这里不能用 if，if 只是在第一次判断时起作用，while 执行循环，直到条件不成立
            # 有放回的随机采样，有一些样本被重复采样，从而在训练集中多次出现，有的则从未在训练集中出现，此则自助采样法。从而保证每棵决策树训练集的差异性
            index = randrange(len(dataset_copy))
            # 将对应索引 index 的内容从 dataset_copy 中导出，并将该内容从 dataset_copy 中删除。
            # pop() 函数用于移除列表中的一个元素（默认最后一个元素），并且返回该元素的值。
            # fold.append(dataset_copy.pop(index))  # 无放回的方式
            fold.append(dataset_copy[index])  # 有放回的方式
        dataset_split.append(fold)
    # 由dataset分割出的n_folds个数据构成的列表，为了用于交叉验证
    return dataset_split

#无放回重抽样
def cross_validation_split_2(dataset, k_folds=4):
    '''Kfold可以将数据随机分成多个数据集

    :returns
       test_kfolds： 生成的 k_folds 训练集
    '''

    from sklearn.model_selection import KFold
    kf = KFold(n_splits=k_folds)

    train_kfolds = list()
    test_kfolds = list()
    for train_index, test_index in kf.split(dataset):
        train, test = dataset[train_index], dataset[test_index]
        train_kfolds.append(train)
        test_kfolds.append(test)
    return train_kfolds, test_kfolds

#留一验证 结果和cross_validation_split_2一致
def cross_validation_split_3(dataset, k_folds):
    '''
    留一验证（每次用一个数据集进行验证，其它数据集进行训练，适合小样本数据）

    :param dataset:
    :param k_folds:
    :return:
    '''
    from sklearn.model_selection import LeaveOneOut

    train_kfolds = list()
    test_kfolds = list()
    # index = list()
    loo = LeaveOneOut()
    for train_index, test_index in loo.split(dataset):
        train, test = dataset[train_index], dataset[test_index]
        train_kfolds.append(train)
        test_kfolds.append(test)
        # index.append(list(test_index))
    # index = sum(index, [])#k个test数据集汇总后，是完整的数据集
    return train_kfolds, test_kfolds

#直接用函数进行交叉验证，便于比较
def cross_val_test(dataset):
    '''
    sklearn.model_selection.cross_val_score进行k折交叉验证
    '''
    from sklearn.model_selection import cross_val_score
    from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier
    from sklearn.tree import DecisionTreeClassifier

    datasetArr = np.array(dataset)
    m, n = datasetArr.shape
    X, y = datasetArr[:, 0:n-1], datasetArr[:, -1]

    #决策树
    dt_clf = DecisionTreeClassifier(criterion="gini", splitter="best", max_depth=None, min_samples_split=2, random_state = 0)
    #随机森林
    rf_clf = RandomForestClassifier(n_estimators=10, max_depth=None, min_samples_split = 2, random_state = 0)
    et_clf = ExtraTreesClassifier(n_estimators=10, max_depth=None, min_samples_split=2, random_state=0)

    for clf, title in zip([dt_clf, rf_clf, et_clf], ['dtc', 'rfc', 'etc']):
        print('模型%s结果 ......' % title)
        scores = cross_val_score(clf, X, y, cv=5)  # 5折集交叉验证
        print('交叉验证得分：', scores)
        print('平均得分： ', scores.mean())