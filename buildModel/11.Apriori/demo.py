import apriori
if __name__ == '__main__':
    # 项目案例
    # 发现毒蘑菇的相似特性
    # 得到全集的数据
    dataSet = [line.split() for line in open("../data/11.Apriori/mushroom.dat").readlines()]
    L, supportData = apriori.apriori(dataSet, minSupport=0.3)

    #生成规则集合
    rules = apriori.generateRules(L, supportData, minConf=0.5)
    print('rules: ', rules)
    # 2表示毒蘑菇，1表示可食用的蘑菇
    # 找出关于2的频繁子项出来，就知道如果是毒蘑菇，那么出现频繁的也可能是毒蘑菇
    for i in range(1,len(L)):
        for item in L[i]:
            if item.intersection({2}):#返回一个新集合，该集合的元素既包含在集合 x 又包含在集合 y 中
                print(item)

