# -*- coding: utf-8 -*-
"""
集成算法：AdaBoost
AdaBoost (adaptive boosting: 自适应 boosting) 
* 优点：泛化（由具体的、个别的扩大为一般的）错误率低，易编码，可以应用在大部分分类器上，无参数调节。
* 缺点：对离群点敏感。
* 适用数据类型：数值型和标称型数据。
"""

### 1.准备数据
#确保类别标签是+1和-1，而非1和0
def loadDataSet(fileName):
    # 获取 feature 的数量, 便于获取
    numFeat = len(open(fileName).readline().split('\t'))
    dataArr = []
    labelArr = []
    fr = open(fileName)
    for line in fr.readlines():
        lineArr = []
        curLine = line.strip().split('\t')
        for i in range(numFeat-1):
            lineArr.append(float(curLine[i]))
        dataArr.append(lineArr)
        labelArr.append(float(curLine[-1]))
    return dataArr, labelArr

### 2 AdaBoost算法实现
'''
对每次迭代
利用buildStump()函数找到最佳的单层决策树
计算 alpha
计算新的权重向量D
更新累计类别估价值
如果错误率等于0.0，则退出循环
'''
####  2.1 利用单层决策树构建弱分类器
'''
伪代码：
将最小错误率minError 设为+inf
对数据集中的每一个特征（第一层循环）：
    对每一个步长（第二层循环）：
        对每一个不等号（第三层循环）：
            建立一个单层决策树并利用加权数据集对他进行测试
            如果错误率低于minError,则将当前单层决策树设为最佳单层决策树
返回最佳单层决策树
'''
import numpy as np
def stumpClassify(dataMat, dimen, threshVal, threshIneq):
    """stumpClassify(将数据集，按照feature列的value进行 二分法切分比较来赋值分类)
    通过阈值比较对数据分类

    Args:
        dataMat    Matrix数据集
        dimen      特征列
        threshVal  特征列要比较的值
        threshIneq threshIneq == 'lt'表示修改左边的值，gt表示修改右边的值  
    Returns:
        retArray 结果集
    """
    # 默认都是1
    retArray = np.ones((dataMat.shape[0], 1))
    # dataMat[:, dimen] 表示数据集中第dimen列的所有值
    # threshIneq == 'lt'表示修改左边的值，gt表示修改右边的值
    # print '-----', threshIneq, dataMat[:, dimen], threshVal
    if threshIneq == 'lt':
        retArray[dataMat[:, dimen] <= threshVal] = -1.0
    else:
        retArray[dataMat[:, dimen] > threshVal] = -1.0
    return retArray

def buildStump(dataArr, labelArr, D):
    """buildStump(得到决策树的模型)
   会遍历stumpClassify()函数所有可能的输入值，并找到数据集上最佳的的单层决策树
   最佳：指的是基于数据的权重向量D来定义的
    Args:
        dataArr   特征标签集合
        labelArr  分类标签集合
        D         最初的样本的所有特征权重集合
    Returns:
        bestStump    最优的分类器模型: {'dim': 0, 'thresh': 0.9, 'ineq': 'lt'},dim：最优特征下标，thresh：特征分割点，ineq：分类方法，小于分割点为-1，还是大于分割点的为-1
        minError     错误率
        bestClasEst  训练后的结果集
    """
    dataMat = np.mat(dataArr)
    labelMat = np.mat(labelArr).T
    # m行 n列
    m, n = dataMat.shape

    # 初始化数据
    numSteps = 10.0 #用于对特征的所有可能值上遍历
    bestStump = {} #用于存储给定权重D时所得到的最佳的那层决策树的相关信息
    bestClasEst = np.mat(np.zeros((m, 1)))
    # 初始化的最小误差为无穷大
    minError = np.inf

    # 循环所有的feature列，将列切分成 若干份，每一段以最左边的点作为分类节点
    for i in range(n):
        rangeMin = dataMat[:, i].min()
        rangeMax = dataMat[:, i].max()
        
        # 计算每一份的元素个数
        stepSize = (rangeMax-rangeMin)/numSteps
        # 例如： 4=(10-1)/2   那么  1-4(-1次)   1(0次)  1+1*4(1次)   1+2*4(2次)
        # 所以： 循环 -1/0/1/2
        for j in range(-1, int(numSteps)+1):
            # go over less than and greater than
            for inequal in ['lt', 'gt']:
                # 如果是-1，那么得到rangeMin-stepSize; 如果是numSteps，那么得到rangeMax
                threshVal = (rangeMin + float(j) * stepSize)
                # 对单层决策树进行简单分类，得到预测的分类值
                predictedVals = stumpClassify(dataMat, i, threshVal, inequal)
                # print predictedVals
                errArr = np.mat(np.ones((m, 1)))
                # 正确为0，错误为1
                errArr[predictedVals == labelMat] = 0
                # 计算 平均每个特征的概率0.2*错误概率的总和为多少，就知道错误率多高
                # 例如： 一个都没错，那么错误率= 0.2*0=0 ， 5个都错，那么错误率= 0.2*5=1， 只错3个，那么错误率= 0.2*3=0.6
                weightedError = D.T*errArr  #加权错误率
                '''
                dim            表示 feature列
                threshVal      表示树的分界值
                inequal        表示计算树左右颠倒的错误率的情况
                weightedError  表示整体结果的错误率
                bestClasEst    预测的最优结果
                '''
                # print "split: dim %d, thresh %.2f, thresh ineqal: %s, the weighted error is %.3f" % (i, threshVal, inequal, weightedError)
                if weightedError < minError:
                    minError = weightedError
                    bestClasEst = predictedVals.copy()
                    bestStump['dim'] = i
                    bestStump['thresh'] = threshVal
                    bestStump['ineq'] = inequal

    # bestStump 表示分类器的结果，在第几个列上，用大于／小于比较，阈值是多少
    return bestStump, minError, bestClasEst

#### 2.2  完整AdaBoost算法实现
'''
对每次迭代：
    利用BuildStump()函数找到最佳单层分类决策树
    将组价单层决策树加入到单层决策树组
    计算alpha
    计算新的权重向量D
    更新累计类别估计值
    如果错误率等于0.0，则退出循环
    
发现：
alpha （模型权重）目的主要是计算每一个分类器实例的权重(加和就是分类结果)
  分类的权重值：最大的值= alpha 的加和，最小值=-最大值
D （样本权重）的目的是为了计算错误概率： weightedError = D.T*errArr，求最佳分类器
  样本的权重值：如果一个值误判的几率越小，那么 D 的样本权重越小
'''
def adaBoostTrainDS(dataArr, labelArr, numIt=10):
    """adaBoostTrainDS(adaBoost训练过程放大)
    Args:
        dataArr   特征标签集合
        labelArr  分类标签集合
        numIt     迭代次数
    Returns:
        weakClassArr  弱分类器的集合
        aggClassEst   预测的分类结果值
    """
    weakClassArr = [] #弱分类器的集合
    m = len(dataArr)#样本数目
    # 初始化 D，设置每个样本的权重值，平均分为m份
    D = np.mat(np.ones((m, 1))/m) #权重一样：1/m
    aggClassEst = np.mat(np.zeros((m, 1)))
    
    for i in range(numIt):
        print("============= 第 %d 次迭代结果 ================="%i)
        # 得到单层决策树模型
        bestStump, error, classEst = buildStump(dataArr, labelArr, D)#  bestStump, error, classEst：单层分类结果，加权最小错误率，样本权重
        # alpha目的主要是计算每一个分类器实例的权重(组合就是分类结果)
        # 计算每个分类器的alpha权重值
        alpha = float(0.5 * np.log((1.0 - error) / max(error, 1e-16)))
        bestStump['alpha'] = alpha
        # store Stump Params in Array
        weakClassArr.append(bestStump)
        print ("alpha=%s, bestStump=%s, error=%s \n" % (alpha, bestStump, error))#error最优加权错误率
        
        #更新D：
        '''D_i+1 = D_i * -alpha/sum(D) #分类正确时
                   D_i * -alpha/sum(D) #分类错误时
        '''
        # 分类正确：乘积为1，不会影响结果，-1主要是下面求e的-alpha次方
        # 分类错误：乘积为 -1，结果会受影响，所以也乘以 -1
        expon = np.multiply(-1*alpha*np.mat(labelArr).T, classEst) #分类正确时为-alpha,错误时为alpha
        print ('(-1取反)预测值expon:', expon.T[0,0:10])
        # 计算e的expon次方，然后计算得到一个综合的概率的值
        # 结果发现： 判断错误的样本，D中相对应的样本权重值会变大。
        D = np.multiply(D, np.exp(expon))
        print('D.sum(): ',D.sum())
        D = D/D.sum()
        
        # 预测的分类结果值，在上一轮结果的基础上，进行加和操作
        print ('当前的分类结果：\n', alpha*classEst.T[0,0:10])
        aggClassEst += alpha*classEst #更新类别估计值
        print ("叠加后的分类结果aggClassEst:\n ", aggClassEst.T[0,0:10])#行矩阵1*m
        # sign 判断正为1， 0为0， 负为-1，通过最终加和的权重值，判断符号。
        # 结果为：错误的样本标签集合，因为是 !=,那么结果就是0 正, 1 负
        aggErrors = np.multiply(np.sign(aggClassEst) != np.mat(labelArr).T, np.ones((m, 1))) #正确为0，错误为1
        errorRate = aggErrors.sum()/m  #aggErrors.sum()分错数目
        print ("total error=%s " % (errorRate) )
        
        if errorRate == 0.0:
            break
    return weakClassArr, aggClassEst

#### 3.测试算法：
def adaClassify(datToClass, classifierArr):
    """adaClassify(ada分类测试)
    Args:
        datToClass     多个待分类的样例
        classifierArr  弱分类器的集合
    Returns:
        sign(aggClassEst) 分类结果
    """
    # do stuff similar to last aggClassEst in adaBoostTrainDS
    dataMat = np.mat(datToClass)
    m = dataMat.shape[0]
    aggClassEst = np.mat(np.zeros((m, 1)))

    # 循环 多个分类器
    for i in range(len(classifierArr)):
        # 前提： 我们已经知道了最佳的分类器的实例
        # 通过分类器来核算每一次的分类结果，然后通过alpha*每一次的结果 得到最后的权重加和的值。
        classEst = stumpClassify(dataMat, classifierArr[i]['dim'], classifierArr[i]['thresh'], classifierArr[i]['ineq'])
        aggClassEst += classifierArr[i]['alpha']*classEst
    return np.sign(aggClassEst)

#### 4. 绘制AUC曲线
def plotROC(predStrengths, classLabels):
    """plotROC(打印ROC曲线，并计算AUC的面积大小)

    Args:
        predStrengths  最终预测结果的权重值
        classLabels    原始数据的分类结果集
    """
#    print('predStrengths=', predStrengths)
#    print('classLabels=', classLabels)

    import matplotlib.pyplot as plt
    # variable to calculate AUC
    ySum = 0.0
    # 对正样本的进行求和
    numPosClas = sum(np.array(classLabels)==1.0)
    # 正样本的概率
    yStep = 1/float(numPosClas)
    # 负样本的概率
    xStep = 1/float(len(classLabels)-numPosClas)
    # argsort函数返回的是数组值从小到大的索引值
    # get sorted index, it's reverse
    sortedIndicies = predStrengths.argsort()
    # 测试结果是否是从小到大排列
    #print('sortedIndicies=', sortedIndicies, predStrengths[0, 176], predStrengths.min(), predStrengths[0, 293], predStrengths.max())

    # 开始创建模版对象
    fig = plt.figure()
    fig.clf()
    ax = plt.subplot(111)
    # cursor光标值(x,y)
    cur = (1.0, 1.0)
    # loop through all the values, drawing a line segment at each point
    for index in sortedIndicies.tolist()[0]:
        if classLabels[index] == 1.0:
            delX = 0
            delY = yStep
        else:
            delX = xStep
            delY = 0
            ySum += cur[1]
        # draw line from cur to (cur[0]-delX, cur[1]-delY)
        # 画点连线 (x1, x2, y1, y2)
        #print(cur[0], cur[0]-delX, cur[1], cur[1]-delY)
        ax.plot([cur[0], cur[0]-delX], [cur[1], cur[1]-delY], c='b')
        cur = (cur[0]-delX, cur[1]-delY)
    #计算AUC面积
    AUC = ySum*xStep
    # 画对角的虚线线
    ax.plot([0, 1], [0, 1], 'b--')
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('ROC curve & AUC = %f'%AUC)
    # 设置画图的范围区间 (x1, x2, y1, y2)
    ax.axis([0, 1, 0, 1])
    plt.show()
    '''
    参考说明：http://blog.csdn.net/wenyusuran/article/details/39056013
    为了计算 AUC ，我们需要对多个小矩形的面积进行累加。
    这些小矩形的宽度是xStep，因此可以先对所有矩形的高度进行累加，最后再乘以xStep得到其总面积。
    所有高度的和(ySum)随着x轴的每次移动而渐次增加。
    '''
def plotROC2(predStrengths,classLabels):
    """
    plotROC(打印ROC曲线，并计算AUC的面积大小)，自己尝试按自己的理解重新绘制了ROC曲线
    
    Args:
        predStrengths  最终预测结果的权重值
        classLabels    原始数据的分类结果集
    """
    #print('predStrengths=', predStrengths)
    #print('classLabels=', classLabels)

    import matplotlib.pyplot as plt
    '''
    纵轴：正样本分对概率  TPR = TP/(TP+FN) :TP+FN为实际正样本个数
    横轴：负样本分错概率  FPR = FN/(FN+TN) :FN+TN为实际负样本个数
    '''
    #实际正样本个数
    posNum = sum(np.array(classLabels) == 1.0)
    
    negNum = len(classLabels) - posNum
    # argsort函数返回的是数组值从小到大的索引值
    #对预测结果排序
    sortedIndicies = predStrengths.argsort()    #matrix: (1, 299)
    sortedIndicies = sortedIndicies.tolist()[0] #转成list
    #
    TPR = list()
    FPR = list()
    #初始TP，FN，FP，TN:此时假设所有样本均分为正样本，则
    TP = posNum
    FN = 0
    FP = negNum
    TN = 0 
    TPR.append(1)
    FPR.append(1)
    
    # loop through all the values, drawing a line segment at each point
    for i in range(len(sortedIndicies)): #遍历 升序排序后的下标
        temp = sortedIndicies[i]
        #预测时，>阈值的为正样本 ,<= 阈值的为负样本
        
        if classLabels[temp] == 1.0:#预测样本为负，实际为正
            TP = TP - 1  #TP被预测为FN了
            FN = FN + 1 
        else:#预测样本为负，实际为负样本
            TN = TN + 1
            FP =  FP - 1
        TPR.append( float(TP) / (TP + FN ) )
        FPR.append(float(FP) / (FP + TN))
        #print('TP=%d , FN= %d, FP=%d, TN=%d '%(TP,FN,FP,TN))
        # draw line from cur to (cur[0]-delX, cur[1]-delY)
        # 画点连线 (x1, x2, y1, y2)
    AUC = 0
    for i in range(len(TPR)-1):
        y1 = TPR[i]
        y2 = TPR[i+1]
        x1 = FPR[i]
        x2 = FPR[i+1]
        AUC += (y1+y2)*(x1-x2)/2.0#计算每个小梯形面积
    print("AUC = %f "%AUC)    
    fig = plt.figure()
    fig.clf()
    ax = plt.subplot(111)
    ax.plot(FPR,TPR,'b-')
    # 画对角的虚线线
    ax.plot([0, 1], [0, 1], 'b--')
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('ROC curve & AUC = %f'%AUC)
    # 设置画图的范围区间 (x1, x2, y1, y2)
    ax.axis([0, 1, 0, 1])
    plt.show()
    
    
 
if __name__ == '__main__':
    # 马疝病数据集
    # 训练集合
    import os 
    os.chdir(r'C:\Users\Administrator\Desktop\ML\7.AdaBoost RF')
    dataArr, labelArr = loadDataSet("horseColicTraining2.txt")
    weakClassArr, aggClassEst = adaBoostTrainDS(dataArr, labelArr, 10)
    print(weakClassArr, '\n-----\n', aggClassEst.T)
    
    # 计算ROC下面的AUC的面积大小
    plotROC(aggClassEst.T, labelArr)
    plotROC2(aggClassEst.T, labelArr)

    # 测试集合
    dataArrTest, labelArrTest = loadDataSet("horseColicTest2.txt")
    m = len(dataArrTest)
    predicting10 = adaClassify(dataArrTest, weakClassArr)
    errArr = np.mat(np.ones((m, 1)))
    # 测试：计算总样本数，错误样本数，错误率
    print("测试集 样本个数：%d, 样本分错个数：%d, 错误率：%f "%(m, errArr[predicting10 != \
                                                       np.mat(labelArrTest).T].sum(), errArr[predicting10 != np.mat(labelArrTest).T].sum()/m) )