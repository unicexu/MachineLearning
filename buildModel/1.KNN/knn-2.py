# -*- coding: utf-8 -*-
"""
机器学习实战--KNN

@author: unice
@time： 2019-04-26

项目概述：手写数字识别系统
构造一个能识别数字 0 到 9 的基于 KNN 分类器的手写数字识别系统。
需要识别的数字是存储在文本文件中的具有相同的色彩和大小：宽高是 32 像素 * 32 像素的黑白图像。

开发流程
收集数据：提供文本文件。
准备数据：编写函数 img2vector(), 将图像格式转换为分类器使用的向量格式
分析数据：在 Python 命令提示符中检查数据，确保它符合要求
训练算法：此步骤不适用于 KNN
测试算法：编写函数使用提供的部分数据集作为测试样本，测试样本与非测试样本的
         区别在于测试样本是已经完成分类的数据，如果预测分类与实际类别不同，
         则标记为一个错误
使用算法：本例没有完成此步骤，若你感兴趣可以构建完整的应用程序，从图像中提取
         数字，并完成数字识别，美国的邮件分拣系统就是一个实际运行的类似系统
    
"""

import numpy as np
def img2vector(filename):
    '''
    函数描述：将图像文本数据转换为向量,原数据为宽高是 32 像素 * 32 像素的黑白图像，转成1*1024的数组
    '''
    returnVect = np.zeros((1,1024))
    fr = open(filename)
    for i in range(32):
        lineStr = fr.readline()
        for j in range(32):
            returnVect[0,32*i+j] = int(lineStr[j])
    return returnVect

def loadData(filename):
    '''
    函数说明：加载文件夹下的所有图片文件，并解析出标签，并将32*32像素文件转成1*1024的数组
    '''
    from os import listdir
    # 1. 导入数据
    hwLabels = [] 
    fileList = listdir(filename) 
    m = len(fileList)                     #文件个数
    dataArray = np.zeros((m,1024))
    # hwLabels存储0～9对应的index位置， trainingMat存放的每个位置对应的图片向量
    for i in range(m):
        fileNameStr = fileList[i]
        fileStr = fileNameStr.strip().split('.')[0]
        classNum = int(fileStr.split('_')[0])
        
        hwLabels.append(classNum)
        # 将 32*32的矩阵->1*1024的矩阵
        dataArray[i, :] = img2vector('%s/%s'%(filename,fileNameStr))
        dataMat = np.mat(dataArray)
    return  dataMat,hwLabels

def autoNorm(dataMat):
    """
    函数说明:
        归一化特征值，消除特征之间量级不同导致的影响
    Parameters:
        dataMat: 数据集矩阵
    Returns:
        归一化后的数据集 normDataSet. ranges和minVals即最小值与范围，并没有用到

    归一化公式：
        Y = (X-Xmin)/(Xmax-Xmin)
        其中的 min 和 max 分别是数据集中的最小特征值和最大特征值。该函数可以自动将数字特征值转化为0到1的区间。
    """
    # 计算每种属性的最大值、最小值、范围
    minVals = dataMat.min(0)
    maxVals = dataMat.max(0)
    # 极差
    ranges = maxVals - minVals
    normDataSet = np.zeros(dataMat.shape)
    m = dataMat.shape[0]
    # 生成与最小值之差组成的矩阵
    normDataSet = dataMat - np.tile(minVals, (m, 1))
    # 将最小值之差除以范围组成矩阵
    normDataSet = normDataSet / np.tile(ranges, (m, 1))  # element wise divide
    return normDataSet, ranges, minVals

def knnClassify(inX, dataSet, labels, k):
    """
    函数说明：knn模型训练
        注意：labels元素数目和dataSet行数相同；程序使用欧式距离公式.
        预测数据所在分类可在输入下列命令
   
    Parameters:
        inX: 用于分类的输入向量
        dataSet: 输入的训练样本集
        labels: 标签向量
        k: 选择最近邻居的数目
    
    Returns:
        maxSortedClass:数目最多的分类
        maxSortedClassCnt：数目最多的分类对应的数量
        
    eg:
    inx = [1,2,3]
    DS = [[1,2,3],[1,2,0]]
    """
    import operator
    # -----------实现的第一种方式----------------------------------------------------------------------------------------------------------------------------
    # 1. 欧式距离计算
    """
    欧氏距离： 点到点之间的距离
       第一行： 同一个点 到 dataSet的第一个点的距离。
       第二行： 同一个点 到 dataSet的第二个点的距离。
       ...
       第N行： 同一个点 到 dataSet的第N个点的距离。
    [[1,2,3],[1,2,3]]-[[1,2,3],[1,2,0]]
    (A1-A2)^2+(B1-B2)^2+(c1-c2)^2
    """
    dataSetSize = dataSet.shape[0]
    # tile生成和训练样本对应的矩阵，并与训练样本求差
    diffMat = np.tile(inX, (dataSetSize, 1)) - dataSet
    # 取平方
    sqDiffMat = np.square(diffMat)
    # 将矩阵的每一行相加
    sqDistances = sqDiffMat.sum(axis=1)
    # 开方
    distances = np.sqrt(sqDistances)
    #将矩阵转换为一位数组
    distances = np.squeeze(distances)   #从数组的形状中删除单维度条目，即把shape中为1的维度去掉
    # 根据距离排序从小到大的排序，返回对应的索引位置
    sortedDistIndicies = distances.argsort()
    #print(sortedDistIndicies)
    # 2. 选择距离最小的k个点
    classCount = {}
    for i in range(k):
        # 找到该样本的类型
        voteIlabel = labels[sortedDistIndicies[0,i]]
        classCount[voteIlabel] = classCount.get(voteIlabel, 0) + 1
        
    sortedClassCount = sorted(classCount.items(), key=operator.itemgetter(1), reverse=True) #获取数目最多的分类结果
    maxSortedClass =  sortedClassCount[0][0] #最多的分类
    #maxSortedClassCnt = sortedClassCount[0][1]#最多分类的数目
    return maxSortedClass 

def handwritingClassTest(trainFilename,testFilename):
    '''
    函数描述：测试算法，编写函数使用提供的部分数据集作为测试样本，如果预测分类与实际类别不同，则标记为一个错误
    
    Parameters:
        trainFilename:训练集文件夹路径
        testFilename:测试集文件夹路径
    '''
    X_train, y_train = loadData(trainFilename)
    # 2. 导入测试数据
    X_test, y_test = loadData(testFilename)
    
    errorCount = 0.0
    y_pred = []
    mTest = len(y_test)
    for i in range(mTest):
        classifierResult = knnClassify(X_test[i],X_train,y_train,3)
        y_pred.append(classifierResult)
        if (classifierResult != y_test[i]): errorCount += 1.0
    print ("训练样本个数%d,测试样本个数：%d" %(len(y_train),len(y_test)))
    print ("\nthe total number of errors is: %d" % errorCount)
    print ("\nthe total error rate is: %f" % (errorCount / float(mTest)) )

if __name__ == '__main':
    import os
    os.getcwd()#获取当前工作路径
    os.chdir(r"C:\Users\Administrator\Desktop\ML-practice\KNN")#设置当前工作路径
    testVector = img2vector('testDigits/0_13.txt')


    handwritingClassTest('trainingDigits','testDigits')