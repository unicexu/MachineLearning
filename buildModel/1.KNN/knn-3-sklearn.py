# -*- coding: utf-8 -*-
"""
使用sklearn.neighbors 训练模型
@author: unice
"""

import os
os.chdir(r"C:\Users\Administrator\Desktop\ML-practice\KNN")#设置当前工作路径
import sys
sys.path.append("..")
import dataUtils as dtUtil#导入自定义工具模块，用于读取文件
from sklearn.neighbors import NearestNeighbors
import numpy as np

def test1():
    X = np.array([[-1, -1], [-2, -1], [-3, -2], [1, 1], [2, 1], [3, 2]])
    nbrs = NearestNeighbors(n_neighbors=2, algorithm='ball_tree').fit(X)
    distances, indices = nbrs.kneighbors(X)   #最近的前2个数据对应的索引和距离
    nbrs.kneighbors_graph(X).toarray()        #生成表示相邻点之间的连接的稀疏图

def test2():
    #KDTree和BallTree类
    from sklearn.neighbors import KDTree
    X = np.array([[-1, -1], [-2, -1], [-3, -2], [1, 1], [2, 1], [3, 2]])
    kdt = KDTree(X, leaf_size=30, metric='euclidean')
    kdt.query(X, k=2, return_distance=False)    

def classification():
    """
    ================================
    Nearest Neighbors Classification
    ================================
    
    Sample usage of Nearest Neighbors classification.
    It will plot the decision boundaries for each class.
    """
    print(__doc__)#输出文件开头注释的内容 
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib.colors import ListedColormap
    from sklearn import neighbors, datasets
    
    n_neighbors = 15
    # import some data to play with
    iris = datasets.load_iris()
    
    # we only take the first two features. We could avoid this ugly
    # slicing by using a two-dim dataset
    X = iris.data[:, :2]
    y = iris.target  #三类
    
    h = .02  # step size in the mesh
    
    # Create color maps
    cmap_light = ListedColormap(['#FFAAAA', '#AAFFAA', '#AAAAFF'])
    cmap_bold = ListedColormap(['#FF0000', '#00FF00', '#0000FF'])
    
    for weights in ['uniform', 'distance']:
        # we create an instance of Neighbours Classifier and fit the data.
        clf = neighbors.KNeighborsClassifier(n_neighbors, weights=weights)
        clf.fit(X, y)
    
        # Plot the decision boundary. For that, we will assign a color to each
        # point in the mesh [x_min, x_max]x[y_min, y_max].
        x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
        y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                             np.arange(y_min, y_max, h))
        Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    
        # Put the result into a color plot
        Z = Z.reshape(xx.shape)
        plt.figure()
        plt.pcolormesh(xx, yy, Z, cmap=cmap_light)
    
        # Plot also the training points
        plt.scatter(X[:, 0], X[:, 1], c=y, cmap=cmap_bold,
                    edgecolor='k', s=20)
        plt.xlim(xx.min(), xx.max())
        plt.ylim(yy.min(), yy.max())
        plt.title("3-Class classification (k = %i, weights = '%s')"
                  % (n_neighbors, weights))
    
    plt.show()
          
 