# -*- coding: utf-8 -*-
"""
机器学习实战--KNN

@author: unice
@time： 2019-04-26

KNN简介：KNN 是一个简单的无显示学习过程，非泛化学习的监督学习模型。在分类和回归中均有应用。
基本原理：简单来说，通过距离度量来计算查询点（query point）与每个训练数据点的距离，然后选出与查询点（query point）相近的K个最邻点（K nearest neighbors），
使用分类决策来选出对应的标签来作为该查询点的标签。

项目概述：
    海伦使用约会网站寻找约会对象。经过一段时间之后，她发现曾交往过三种类型的人:

    不喜欢的人
    魅力一般的人
    极具魅力的人
    她希望：
        工作日与魅力一般的人约会
        周末与极具魅力的人约会
        不喜欢的人则直接排除掉
    现在她收集到了一些约会网站未曾记录的数据信息，这更有助于匹配对象的归类。
 
开发流程：
    收集数据：提供文本文件
    准备数据：使用 Python 解析文本文件
    分析数据：使用 Matplotlib 画二维散点图
    训练算法：此步骤不适用于 k-近邻算法
    测试算法：使用海伦提供的部分数据作为测试样本。
            测试样本和非测试样本的区别在于：
                测试样本是已经完成分类的数据，如果预测分类与实际类别不同，则标记为一个错误。
    使用算法：产生简单的命令行程序，然后海伦可以输入一些特征数据以判断对方是否为自己喜欢的类型。
"""

import numpy as np

def loadData(fileName):
    '''
    函数说明：加载文本数据,并解析出特征和标签
    
    Parameters:
        fileName:文件名
    Returns:
        dataMat:特征数据
        labelList:标签数据
    '''
    dataMat = []
    labelList = []
    fr = open(fileName)
    for lines in fr.readlines():
        temp  = lines.strip().split('\t')
        temp_data = temp[0:len(temp)-1]
        temp_data = list(map(float,temp_data))#将特征数据转为float类型
        dataMat.append(temp_data)
        labelList.append(int(temp[-1]))
    fr.close()  
    dataMat = np.mat(dataMat)
    return dataMat,labelList

def plotData(dataMat,labelList):
    '''
    函数说明：按样本类别对前两列数据进行绘图
    
    Parameters:
        dataMat:特征矩阵
        labelList：标签列表数据
    Returns:
        无
    '''
    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax = fig.add_subplot(111)
    #根据样本标签填充颜色
    temp = [[15.0*n] for n in labelList]
    ax.scatter(dataMat[:, 0].tolist(), dataMat[:, 1].tolist(), s = temp, c = temp) #绘制第1列和第二列的数据
    plt.title('dating data')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()
    
def plotData2(dataMat,labelList):
    '''
    函数说明：按样本类别对前两列数据进行绘图，并加入图例
    
    Parameters:
        dataMat:特征矩阵
        labelList：标签列表数据
    Returns:
        无
    '''
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    
    myfont = mpl.font_manager.FontProperties(fname=r"c:\windows\fonts\simsun.ttc", size=14)#处理中文乱码
    fig = plt.figure()
    axes = plt.subplot(111)
    
    type1_x = []
    type1_y = []
    type2_x = []
    type2_y = []
    type3_x = []
    type3_y = []
 
    for i in range(len(labelList)):
        if labelList[i] == 1:
            type1_x.append(dataMat[i,0])
            type1_y.append(dataMat[i,1])
     
        if labelList[i] == 2:
            type2_x.append(dataMat[i,0])
            type2_y.append(dataMat[i,1])
     
        if labelList[i] == 3:
            type3_x.append(dataMat[i,0])
            type3_y.append(dataMat[i,1])
     
    type1 = axes.scatter(type1_x, type1_y, s=20, c='r')
    type2 = axes.scatter(type2_x, type2_y, s=40, c='b')
    type3 = axes.scatter(type3_x, type3_y, s=60, c='k')
 
    plt.legend((type1, type2, type3), ('不喜欢', '魅力一般', '极具魅力'), prop = myfont)
    plt.title('dating data')
    plt.show()
    
    
def autoNorm(dataMat):
    """
    函数说明:
        归一化特征值，消除特征之间量级不同导致的影响
    Parameters:
        dataMat: 数据集矩阵
    Returns:
        归一化后的数据集 normDataSet. ranges和minVals即最小值与范围，并没有用到

    归一化公式：
        Y = (X-Xmin)/(Xmax-Xmin)
        其中的 min 和 max 分别是数据集中的最小特征值和最大特征值。该函数可以自动将数字特征值转化为0到1的区间。
    """
    # 计算每种属性的最大值、最小值、范围
    minVals = dataMat.min(0)
    maxVals = dataMat.max(0)
    # 极差
    ranges = maxVals - minVals
    normDataSet = np.zeros(dataMat.shape)
    m = dataMat.shape[0]
    # 生成与最小值之差组成的矩阵
    normDataSet = dataMat - np.tile(minVals, (m, 1))
    # 将最小值之差除以范围组成矩阵
    normDataSet = normDataSet / np.tile(ranges, (m, 1))  # element wise divide
    return normDataSet, ranges, minVals

def knnClassify(inX, dataSet, labels, k):
    """
    函数说明：knn模型训练
        注意：labels元素数目和dataSet行数相同；程序使用欧式距离公式.
        预测数据所在分类可在输入下列命令
   
    Parameters:
        inX: 用于分类的输入向量
        dataSet: 输入的训练样本集
        labels: 标签向量
        k: 选择最近邻居的数目
    
    Returns:
        maxSortedClass:数目最多的分类
        maxSortedClassCnt：数目最多的分类对应的数量
        
    eg:
    inx = [1,2,3]
    DS = [[1,2,3],[1,2,0]]
    """
    import operator
    # -----------实现的第一种方式----------------------------------------------------------------------------------------------------------------------------
    # 1. 欧式距离计算
    """
    欧氏距离： 点到点之间的距离
       第一行： 同一个点 到 dataSet的第一个点的距离。
       第二行： 同一个点 到 dataSet的第二个点的距离。
       ...
       第N行： 同一个点 到 dataSet的第N个点的距离。
    [[1,2,3],[1,2,3]]-[[1,2,3],[1,2,0]]
    (A1-A2)^2+(B1-B2)^2+(c1-c2)^2
    """
    dataSetSize = dataSet.shape[0]
    # tile生成和训练样本对应的矩阵，并与训练样本求差
    diffMat = np.tile(inX, (dataSetSize, 1)) - dataSet
    # 取平方
    sqDiffMat = np.square(diffMat)
    # 将矩阵的每一行相加
    sqDistances = sqDiffMat.sum(axis=1)
    # 开方
    distances = np.sqrt(sqDistances)
    #将矩阵转换为一位数组
    distances = np.squeeze(distances)   #从数组的形状中删除单维度条目，即把shape中为1的维度去掉
    # 根据距离排序从小到大的排序，返回对应的索引位置
    sortedDistIndicies = distances.argsort()
    #print(sortedDistIndicies)
    # 2. 选择距离最小的k个点
    classCount = {}
    for i in range(k):
        # 找到该样本的类型
        voteIlabel = labels[sortedDistIndicies[0,i]]
        classCount[voteIlabel] = classCount.get(voteIlabel, 0) + 1
        
    sortedClassCount = sorted(classCount.items(), key=operator.itemgetter(1), reverse=True) #获取数目最多的分类结果
    maxSortedClass =  sortedClassCount[0][0] #最多的分类
    maxSortedClassCnt = sortedClassCount[0][1]#最多分类的数目
    return maxSortedClass,maxSortedClassCnt

def knnClassifyModelTest(fileName,k,hoRatio):
    """
    Desc:
        用KNN对约会网站数据进行分类
    parameters:
        fileName:文件路径
        k:knn参数中选取的k值
        hoRatio:测试样本比例
    return:
        错误数
    """
    # 从文件中加载数据
    datingDataMat, datingLabels = loadData(fileName)  # load data setfrom file
    # 归一化数据
    normMat, ranges, minVals = autoNorm(datingDataMat)
    # m 表示数据的行数，即矩阵的第一维
    m = normMat.shape[0]
    # 设置测试的样本数量， numTestVecs:m表示训练样本的数量
    numTestVecs = int(m * hoRatio)
    print('numTestVecs=', numTestVecs)
    errorCount = 0.0
    
    #前m个为测试数据
    for i in range(numTestVecs):
        # 对数据测试
        classifierResult,classCnt = knnClassify(normMat[i, :], normMat[numTestVecs:m, :], datingLabels[numTestVecs:m],k)
        print ("最大分类结果为：%d,实际分类结果为: %d" % (classifierResult, datingLabels[i]))
        if (classifierResult != datingLabels[i]):
            errorCount += 1.0 #分类错误数
    print("错误率为 : %f" % (errorCount / float(numTestVecs)) )
    
def classifyPerson():
    '''
    函数描述：手动输入数据，然后对其进行分类，看是否进行约会
    
    Parameters:
        无
    Returns:
        无
    '''
    #手动输入数据
    resultList = ['not at all', 'in small doses', 'in large doses']
    percentTats = float(input("percentage of time spent playing video games ?"))
    ffMiles = float(input("frequent filer miles earned per year?"))
    iceCream = float(input("liters of ice cream consumed per year?"))
    datingDataMat, datingLabels = loadData('datingSet.txt') #加载数据
    normMat, ranges, minVals = autoNorm(datingDataMat)#数据标准化处理
    inArr = np.array([ffMiles, percentTats, iceCream])
    normInArr = (inArr-minVals)/ranges   #数据进行标准化处理，实际测试数据怎么标准化
    classifierResult,classifierResultCnt = knnClassify(normInArr,normMat,datingLabels, 3)
    print("You will probably like this person: ", resultList[classifierResult - 1])

if __name__ == '__main':
    import os
    os.getcwd()#获取当前工作路径
    os.chdir(r"C:\Users\Administrator\Desktop\ML-practice\KNN")#设置当前工作路径
    
    #加载数据，并进行绘图
    dataMat,labelList = loadData('datingSet.txt')
    #plotData(dataMat,labelList)
    plotData2(dataMat,labelList)
    
    #min-max归一化处理
    normDataMat = np.mat( np.zeros((len(dataMat),dataMat.shape[1])) )
    ranges = []
    minVals = []
    for i in range(dataMat.shape[1]):
        normDataMat[:,i],ranges1, minVals1 = autoNorm(dataMat[:,i])
        ranges.append(ranges1) 
        minVals.append(minVals1)
        
    #约会数据建立模型
    knnClassifyModelTest('datingSet.txt', k = 10, hoRatio = 0.1)
    
    #输入一组数据, 对数据进行分类
    classifyPerson()
    