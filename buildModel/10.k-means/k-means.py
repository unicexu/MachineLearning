#!/usr/bin/python
# coding:utf8
from __future__ import print_function
from numpy import *
import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt

# 从文本中构建矩阵，加载文本文件，然后处理
def loadDataSet(fileName):  # 通用函数，用来解析以 tab 键分隔的 floats（浮点数）
    dataSet = []
    fr = open(fileName)
    for line in fr.readlines():
        curLine = line.strip().split('\t')
        fltLine = [float(i) for i in curLine] # 映射所有的元素为 float（浮点数）类型
        dataSet.append(fltLine)
    return dataSet

# 计算两个向量的欧式距离（可根据场景选择）
def distEclud(vecA, vecB):
    return sqrt(sum(power(vecA - vecB, 2)))  # la.norm(vecA-vecB)

# 为给定数据集构建一个包含 k 个随机质心的集合。随机质心必须要在整个数据集的边界之内，这可以通过找到数据集每一维的最小和最大值来完成。然后生成 0~1.0 之间的随机数并通过取值范围和最小值，以便确保随机点在数据的边界之内。
def randCent(dataMat, k):
    n = shape(dataMat)[1]  # 列的数量
    centroids = mat(zeros((k, n)))  # 创建k个质心矩阵
    for j in range(n):  # 创建随机簇质心，并且在每一维的边界内
        minJ = min(dataMat[:, j])  # 最小值
        rangeJ = float(max(dataMat[:, j]) - minJ)  # 范围 = 最大值 - 最小值
        centroids[:, j] = mat(minJ + rangeJ * random.rand(k, 1))  # 随机生成
    return centroids

# k-means 聚类算法
# 该算法会创建k个质心，然后将每个点分配到最近的质心，再重新计算质心。
# 这个过程重复数次，知道数据点的簇分配结果不再改变位置。
# 运行结果（多次运行结果可能会不一样，可以试试，原因为随机质心的影响，但总的结果是对的， 因为数据足够相似，也可能会陷入局部最小值）
def kMeans(dataMat, k, distMeas=distEclud, createCent=randCent):
    m = shape(dataMat)[0]  # 行数
    clusterAssment = mat(zeros((m, 2)))  # 创建一个与 dataMat 行数一样，但是有两列的矩阵，用来保存簇分配结果:第一列：质心下标，第二列：最小距离的平方
    centroids = createCent(dataMat, k)  # 创建质心，随机k个质心
    clusterChanged = True
    while clusterChanged:
        clusterChanged = False
        for i in range(m):  # 循环每一个数据点并分配到最近的质心中去
            minDist = inf
            minIndex = -1
            for j in range(k): #计算点到质心的距离，找出最近的质心minIndex，以及对应距离minDist
                distJI = distMeas(centroids[j, :],
                                  dataMat[i, :])  # 计算数据点到质心的距离
                if distJI < minDist:  # 如果距离比 minDist（最小距离）还小，更新 minDist（最小距离）和最小质心的 index（索引）
                    minDist = distJI
                    minIndex = j
            if clusterAssment[i, 0] != minIndex:  # 簇分配结果改变
                clusterChanged = True  # 簇改变
                clusterAssment[i, :] = minIndex, minDist**2  # 更新簇分配结果为最小质心的 index（索引），minDist（最小距离）的平方
        print('质心...... ', centroids)

        for cent in range(k):  # 更新质心
            ptsInClust = dataMat[nonzero(clusterAssment[:, 0].A == cent)[0]]  # 获取该簇中的所有点
            centroids[cent, :] = mean(
                ptsInClust, axis=0)  # 将质心修改为簇中所有点的平均值，mean 就是求平均值的
    return centroids, clusterAssment

# 二分 KMeans 聚类算法, 基于 kMeans 基础之上的优化，以避免陷入局部最小值
def biKMeans(dataMat, k, distMeas=distEclud):
    m = shape(dataMat)[0]
    clusterAssment = mat(zeros((m, 2)))  # 保存每个数据点的簇分配结果和平方误差
    centroid0 = mean(dataMat, axis=0).tolist()[0]  # 质心初始化为所有数据点的均值
    centList = [centroid0]  # 初始化只有 1 个质心的 list
    for j in range(m):  # 计算所有数据点到初始质心的距离平方误差
        clusterAssment[j, 1] = distMeas(mat(centroid0), dataMat[j, :])**2
    while (len(centList) < k):  # 当质心数量小于 k 时
        lowestSSE = inf
        for i in range(len(centList)):  # 对每一个质心
            ptsInCurrCluster = dataMat[nonzero(
                clusterAssment[:, 0].A == i)[0], :]  # 获取当前簇 i 下的所有数据点
            centroidMat, splitClustAss = kMeans(
                ptsInCurrCluster, 2, distMeas)  # 将当前簇 i 进行二分 kMeans 处理
            sseSplit = sum(splitClustAss[:, 1])  # 将二分 kMeans 结果中的平方和的距离进行求和
            sseNotSplit = sum(
                clusterAssment[nonzero(clusterAssment[:, 0].A != i)[0],
                               1])  # 将未参与二分 kMeans 分配结果中的平方和的距离进行求和
            print("sseSplit, and notSplit: ", sseSplit, sseNotSplit)
            if (sseSplit + sseNotSplit) < lowestSSE:
                bestCentToSplit = i
                bestNewCents = centroidMat
                bestClustAss = splitClustAss.copy()
                lowestSSE = sseSplit + sseNotSplit
        # 找出最好的簇分配结果
        bestClustAss[nonzero(bestClustAss[:, 0].A == 1)[0], 0] = len(
            centList)  # 调用二分 kMeans 的结果，默认簇是 0,1. 当然也可以改成其它的数字
        bestClustAss[nonzero(bestClustAss[:, 0].A == 0)[0],
                     0] = bestCentToSplit  # 更新为最佳质心
        print('the bestCentToSplit is: ', bestCentToSplit)
        print('the len of bestClustAss is: ', len(bestClustAss))
        # 更新质心列表
        centList[bestCentToSplit] = bestNewCents[0, :].tolist()[
            0]  # 更新原质心 list 中的第 i 个质心为使用二分 kMeans 后 bestNewCents 的第一个质心
        centList.append(
            bestNewCents[1, :].tolist()[0])  # 添加 bestNewCents 的第二个质心
        clusterAssment[nonzero(clusterAssment[:, 0].A == bestCentToSplit)[
            0], :] = bestClustAss  # 重新分配最好簇下的数据（质心）以及SSE
    return mat(centList), clusterAssment

#sklearn实现
def sklearn_kmeans(dataMat, k):
   # kmeans++

   kmeans = KMeans(n_clusters=k, random_state=0).fit(dataMat)
   labels = kmeans.labels_

   # 模型效果评估
   from sklearn import  metrics
   n_samples, n_features = dataMat.shape  # 总样本量，总特征数
   inertias = kmeans.inertia_  # 样本距离最近的聚类中心的总和
   print('SSE:', inertias)
   # adjusted_rand_s = metrics.adjusted_rand_score(y_true, y_pre)  # 调整后的兰德指数
   # homogeneity_s = metrics.homogeneity_score(y_true, y_pre)  # 同质化得分
   silhouette_s = metrics.silhouette_score(dataMat, labels, metric='euclidean')  # 平均轮廓系数
   print('平均轮廓系数：', silhouette_s)

   #预测聚类结果
   # y_pred = kmeans.predict(dataMat)

   # 模型效果可视化
   centers = kmeans.cluster_centers_ #k个质心# 各类别中心
   colors_arr = np.array(['#4EACC5', '#FF9C34', '#4E9A06', '#CC3299', '#D8D8BF',\
                   '#70DB93', '#5C4033', '#9932CD', '#871F78', '#DBDB70']) # 设置不同类别的颜色
   colors = list(colors_arr[0:k])
   plt.figure()  # 建立画布
   for i in range(k):  # 循环读取类别
       index_sets = np.where(labels == i)  # 找到相同类的索引集合、
       cluster = dataMat[index_sets, :]  # 将相同类的数据划分为一个聚类子集
       plt.scatter(cluster[:, 0], cluster[:, 1], c=colors[i], marker='.')  # 展示聚类子集内的样本点
       plt.plot(centers[i][0], centers[i][1], 'o', markerfacecolor=colors[i]
                , markeredgecolor='k', markersize=6)  # 展示各聚类子集的中心
   plt.show()

#K-Means聚类最优k值的选取
#肘部法则
# clustering dataset
# determine k using elbow method
def elbow(X):
    '''
    1.1
    基本原理：
    手肘法的核心指标是SSE(sum of the squared errors，误差平方和)，公式为：
    K - MEANS聚类算法，K值最优取值

    手肘法的核心思想是：随着聚类数k的增大，样本划分会更加精细，每个簇的聚合程度会逐渐提高，
    那么误差平方和SSE自然会逐渐变小。并且，当k小于真实聚类数时，
    由于k的增大会大幅增加每个簇的聚合程度，故SSE的下降幅度会很大，
    而当k到达真实聚类数时，再增加k所得到的聚合程度回报会迅速变小，
    所以SSE的下降幅度会骤减，然后随着k值的继续增大而趋于平缓，
    也就是说SSE和k的关系图是一个手肘的形状，而这个肘部对应的k值就是数据的真实聚类数。
    当然，这也是该方法被称为手肘法的原因。

    1.2
    实现代码
    我们对数据.利用手肘法选取最佳聚类数k。
    具体做法是让k从1开始取值直到取到你认为合适的上限(
        一般来说这个上限不会太大，这里我们选取上限为10)，
        对每一个k值进行聚类并且记下对于的SSE，然后画出k和SSE的关系图
        （毫无疑问是手肘形），最后选取肘部对应的k作为我们的最佳聚类数。python实现如下：
    '''
    x1 = X[:, 0]
    x2 = X[:, 1]

    plt.plot()
    plt.xlim([0, 10])
    plt.ylim([0, 10])
    plt.title('Dataset')
    plt.scatter(x1, x2)
    plt.show()

    '利用SSE选择k'
    SSE = []  # 存放每次结果的误差平方和
    for k in range(1, 11):
        estimator = KMeans(n_clusters=k)  # 构造聚类器
        estimator.fit(X)
        SSE.append(estimator.inertia_)
    X = range(1, 11)
    plt.xlabel('k')
    plt.ylabel('SSE')
    plt.plot(X, SSE, 'o-')
    plt.show()

#平均轮廓系数
def silhouette_score(dataMat):
    import pandas as pd
    from sklearn.cluster import KMeans
    from sklearn.metrics import silhouette_score
    import matplotlib.pyplot as plt
    from matplotlib.font_manager import FontProperties

    font = FontProperties(fname='/System/Library/Fonts/PingFang.ttc')
    Scores = []  # 存放轮廓系数
    for k in range(2, 11):
        estimator = KMeans(n_clusters=k)  # 构造聚类器
        estimator.fit(dataMat)
        Scores.append(silhouette_score(dataMat, estimator.labels_, metric='euclidean'))
    X = range(2, 11)
    plt.xlabel('k')
    plt.ylabel('轮廓系数', fontproperties = font)
    plt.plot(X, Scores, 'o-')
    plt.show()

def testBasicFunc():
    # 加载测试数据集
    dataMat = mat(loadDataSet('../data/10.KMeans/testSet2.txt'))

    # 测试 randCent() 函数是否正常运行。
    # 首先，先看一下矩阵中的最大值与最小值
    print('min(dataMat[:, 0])=', min(dataMat[:, 0]))
    print('min(dataMat[:, 1])=', min(dataMat[:, 1]))
    print('max(dataMat[:, 1])=', max(dataMat[:, 1]))
    print('max(dataMat[:, 0])=', max(dataMat[:, 0]))

    # 然后看看 randCent() 函数能否生成 min 到 max 之间的值
    print('randCent(dataMat, 2)=', randCent(dataMat, 2))

    # 最后测试一下距离计算方法
    print(' distEclud(dataMat[0], dataMat[1])=', distEclud(dataMat[0], dataMat[1]))


def testKMeans():
    # 加载测试数据集
    dataMat = mat(loadDataSet('../data/10.KMeans/testSet2.txt'))

    # 该算法会创建k个质心，然后将每个点分配到最近的质心，再重新计算质心。
    # 这个过程重复数次，知道数据点的簇分配结果不再改变位置。
    # 运行结果（多次运行结果可能会不一样，可以试试，原因为随机质心的影响，但总的结果是对的， 因为数据足够相似）
    myCentroids, clustAssing = kMeans(dataMat, 3)
    print('k-means.................')
    print('centroids = ', myCentroids)
    print('样本分类：', clustAssing[0:10, :])
    return

def testBiKMeans():
    # 加载测试数据集
    dataMat = mat(loadDataSet('../data/10.KMeans/testSet2.txt'))

    centList, myNewAssments = biKMeans(dataMat, 3)

    print('二分k-means...............')
    print('centList=', centList)
    print('样本分类：', myNewAssments[0:10, :])

if __name__ == "__main__":

    # 测试基础的函数
    # testBasicFunc()

    # # 测试 kMeans 函数
    # testKMeans()
    #
    # # 测试二分 biKMeans 函数
    # testBiKMeans()

    #k值的选择
    #肘部法
    dataMat = loadDataSet('../data/10.KMeans/testSet.txt')
    dataMat = np.array(dataMat)
    elbow(dataMat)
    #k=3
    sklearn_kmeans(dataMat, 4)
    #轮廓系数法求k
    silhouette_score(dataMat)