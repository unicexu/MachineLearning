# -*- coding: utf-8 -*-
"""
sklearn：逻辑回归
"""
from sklearn.datasets import load_iris
from sklearn.linear_model import LogisticRegression
    
'''
官方例子
'''

def sklearnLRTest():
    X, y = load_iris(return_X_y=True)
    clf = LogisticRegression(random_state=0, solver='lbfgs',multi_class='multinomial').fit(X, y)
    clf.predict(X[:2, :]) #类别预测
    clf.predict_proba(X[:2, :]) #概率预测
    clf.score(X, y)
    
"""
函数说明:使用Sklearn构建Logistic回归分类器

Parameters:
	无
Returns:
	无
"""
def colicSklearn():
	frTrain = open(r'C:\Users\Administrator\Desktop\ML\logistic\horseColicTraining.txt')										#打开训练集
	frTest = open(r'C:\Users\Administrator\Desktop\ML\logistic\horseColicTest.txt')											#打开测试集
	trainingSet = []; trainingLabels = []
	testSet = []; testLabels = []
	for line in frTrain.readlines():
		currLine = line.strip().split('\t')
		lineArr = []
		for i in range(len(currLine)-1):
			lineArr.append(float(currLine[i]))
		trainingSet.append(lineArr)
		trainingLabels.append(float(currLine[-1]))
	for line in frTest.readlines():
		currLine = line.strip().split('\t')
		lineArr =[]
		for i in range(len(currLine)-1):
			lineArr.append(float(currLine[i]))
		testSet.append(lineArr)
		testLabels.append(float(currLine[-1]))
	classifier = LogisticRegression(solver = 'sag',max_iter = 500).fit(trainingSet, trainingLabels)
	test_accurcy = classifier.score(testSet, testLabels) * 100
	print('LogisticRegression正确率:%f%%' % test_accurcy)
    
if __name__ == '__main__':
    colicSklearn()