# -*- coding:UTF-8 -*-
import numpy as np
import matplotlib.pyplot as plt
#设置画图字体
from matplotlib.font_manager import FontProperties
def load_data():
    np.random.seed(0)    # 每次随机数固定
    N=100                #每类100个点
    D=2                  #每个点，(x,y)
    K=3                  #3类
    X=np.zeros((300,2))
    Y=np.zeros((300,1))
    for i in range(K):
        ix = range(N * i, N * (i + 1))   #0-100、100-200、200-300
        r = np.linspace(0.0, 1, N)  # radius
        t = np.linspace(i * 4, (i + 1) * 4, N) + np.random.randn(N) * 0.2
        X[ix] = np.c_[r * np.sin(t), r * np.cos(t)]
        Y[ix] = i
    print(X[:, 0].shape)
    #注意：这里的c=Y，Y要转换为不规则写法，s表示点的大小，cmap表示类型
    plt.scatter(X[:, 0], X[:, 1], c=Y.squeeze(), s=30, cmap=plt.cm.Spectral)
    #plt.scatter(X[:, 0], X[:, 1], c=Y.ravel(), s=30, cmap=plt.cm.Spectral)
    #plt.scatter(X[:, 0], X[:, 1], c=Y.reshape(300), s=30, cmap=plt.cm.Spectral)
    plt.show()
    #X.T和X.reshape(2,300)不一样
    #Y.T和Y.reshpe(1,300)是一样的
    #还一个是数据类型转换，Y在传播过程中要当下表使用,计算损失函数
    return D,K,X.T,Y.reshape(1,300).astype(np.int32)
def plot_answer(model,X,Y):
    x_min, x_max = X[0, :].min() - 1, X[0, :].max() + 1
    y_min, y_max = X[1, :].min() - 1, X[1, :].max() + 1
    h = 0.01
    # xx和yy是两个大小相等的矩阵
    # xx和yy是提供坐标（xx,yy）
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    # 列扩展，每个点是一行
    Z = model(np.c_[xx.ravel(),yy.ravel()].T)
    Z = Z.reshape(xx.shape)
    # 轮廓,等高线，预测值相等的点描绘成一个轮廓，把图像进行分割
    # contour   没有颜色填充，只是分割
    # contourf  会进行颜色填充
    plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
    font = FontProperties(fname=r"c:\windows\fonts\simsun.ttc", size=14)
    plt.ylabel(u'y轴', fontproperties=font)
    plt.xlabel(u'x轴', fontproperties=font)
    plt.scatter(X[0, :], X[1, :], c=Y.squeeze(), s=30, cmap=plt.cm.Spectral)
    plt.show()
def propagation():
    M=X.shape[1]
    reg=0.001
    rate=1.0
    W=0.01*np.random.randn(K,D)
    B=np.zeros((K,1))
    for i in range(200):
        Z=np.dot(W,X)+B
        A=np.exp(Z)
        A=A/np.sum(A,axis=0,keepdims=True)
        L=-np.log(A[Y,range(M)])         #实际标签对应的值求log、
        loss=np.sum(L)/M + 0.5*reg*np.sum(W*W)
        if(i%10==0):
            print("iteration %d: loss %f" % (i, loss))
        A[Y, range(M)] -=1
        dw=np.dot(A,X.T)/M
        dw+=reg*W
        db=np.sum(A,axis=1,keepdims=True)/M
        W -= rate * dw
        B -= rate * db
    Z=np.dot(W,X)+B
    predict=np.argmax(Z,axis=0)
    print('training accuracy: %.0f%%' % (np.mean(predict == Y)*100))
    return W,B
def predict(X):
    Z=np.dot(W,X)+B
    ans=np.argmax(Z,axis=0)
    return ans


D,K,X,Y=load_data()
W,B=propagation()
plot_answer(lambda x: predict(x),X,Y)