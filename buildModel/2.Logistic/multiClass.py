# -*- coding: utf-8 -*-
"""
Created on Wed Apr  3 11:21:58 2019

@author: 逻辑回归多分类，softmax多分类
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def sigmoid(h):
    return 1.0 / (1.0 + np.exp(-h))


'''
函数说明:绘制sigmoid函数

Parameters:
	无
Returns:
	无
'''
def plotSigmoid():
    h = np.arange(-10, 10, 0.1) # 定义x的范围，像素为0.1
    s_h = sigmoid(h) # sigmoid为上面定义的函数
    plt.plot(h, s_h)
    plt.axvline(0.0, color='k') # 在坐标轴上加一条竖直的线，0.0为竖直线在坐标轴上的位置
    plt.axhspan(0.0, 1.0, facecolor='1.0', alpha=1.0, ls='dotted') # 加水平间距通过坐标轴
    plt.axhline(y=0.5, ls='dotted', color='k') # 加水线通过坐标轴
    plt.yticks([0.0, 0.5, 1.0]) # 加y轴刻度
    plt.ylim(-0.1, 1.1) # 加y轴范围
    plt.xlabel('h')
    plt.ylabel('$S(h)$')
    plt.title('sigmoid function')
    plt.show()

'''
鸢尾花绘图
'''
def plotIris():
    df = pd.read_csv(r'C:\Users\Administrator\Desktop\ML\logistic\iris.csv',sep=',') # 加载Iris数据集作为DataFrame对象
    X = df.iloc[:, [0, 2]].values # 取出2个特征，并把它们用Numpy数组表示
    plt.scatter(X[:50, 0], X[:50, 1],color='red', marker='o', label='setosa') # 前50个样本的散点图
    plt.scatter(X[50:100, 0], X[50:100, 1],color='blue', marker='x', label='versicolor') # 中间50个样本的散点图
    plt.scatter(X[100:, 0], X[100:, 1],color='green', marker='+', label='Virginica') # 后50个样本的散点图
    plt.xlabel('petal length')
    plt.ylabel('sepal length')
    plt.legend(loc=2) # 把说明放在左上角，具体请参考官方文档
    plt.show()
    
    
def sklearnTest():
    from sklearn import datasets,metrics
    import numpy as np
    from sklearn.cross_validation import train_test_split

    iris = datasets.load_iris() # 由于Iris是很有名的数据集，scikit-learn已经原生自带了。
    X = iris.data[:, [2, 3]]
    y = iris.target # 标签已经转换成0，1，2了
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0) #训练集和测试集
    
    # 为了追求机器学习和最优化算法的最佳性能，我们将特征缩放
    from sklearn.preprocessing import StandardScaler
    sc = StandardScaler()
    sc.fit(X_train) # 估算每个特征的平均值和标准差
    sc.mean_ # 查看特征的平均值，由于Iris我们只用了两个特征，所以结果是array([ 3.82857143,  1.22666667])
    sc.scale_ # 查看特征的标准差，这个结果是array([ 1.79595918,  0.77769705])
    X_train_std = sc.transform(X_train)
    # 注意：这里我们要用同样的参数来标准化测试集，使得测试集和训练集之间有可比性
    X_test_std = sc.transform(X_test)
    
    # 训练感知机模型
    from sklearn.linear_model import Perceptron
    # n_iter：可以理解成梯度下降中迭代的次数
    # eta0：可以理解成梯度下降中的学习率
    # random_state：设置随机种子的，为了每次迭代都有相同的训练集顺序
    ppn = Perceptron(n_iter=40, eta0=0.1, random_state=0)
    ppn.fit(X_train_std, y_train)
    
    # 分类测试集，这将返回一个测试结果的数组
    y_pred = ppn.predict(X_test_std)
    # 计算模型在测试集上的准确性，我的结果为0.9，还不错
    metrics.accuracy_score(y_test, y_pred)


'''
scikit-learn训练逻辑回归模型
'''
def sklearn-LR():
    from sklearn import datasets
    import numpy as np
    from sklearn.cross_validation import train_test_split
    
    iris = datasets.load_iris()
    X = iris.data[:, [2, 3]]
    y = iris.target
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)
    
    from sklearn.preprocessing import StandardScaler
    sc = StandardScaler()
    sc.fit(X_train)
    X_train_std = sc.transform(X_train)
    X_test_std = sc.transform(X_test)
    
    X_combined_std = np.vstack((X_train_std, X_test_std))
    y_combined = np.hstack((y_train, y_test))
    
    from sklearn.linear_model import LogisticRegression
    lr = LogisticRegression(C=1000.0, random_state=0)
    lr.fit(X_train_std, y_train)
    testPred = lr.predict_proba(X_test_std) # 查看第一个测试样本属于各个类别的概率
    plot_decision_regions(X_combined_std, y_combined, classifier=lr, test_idx=range(105,150))
    plt.xlabel('petal length [standardized]')
    plt.ylabel('petal width [standardized]')
    plt.legend(loc='upper left')
    plt.show()

def plot_decision_regions(X, y, classifier, test_idx=None, resolution=0.02):
    import matplotlib.pyplot as plt
    import numpy as np
    from matplotlib.colors import ListedColormap
    # setup marker generator and color map
    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])
    # plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution), np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.4, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())
    # plot class samples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0], y=X[y == cl, 1],alpha=0.8, c=cmap(idx),marker=markers[idx], label=cl)
    # highlight test samples
    if test_idx:
        X_test, y_test = X[test_idx, :], y[test_idx]
        plt.scatter(X_test[:, 0], X_test[:, 1], c='', alpha=1.0, linewidth=1, marker='o', s=55, label='test set')
        
        
        