# -*- coding: utf-8 -*-
"""
LWLR:局部加权线性回归
给预测点附近的每个点赋予一定的权重
每次预测都选出对应的预测子集
常使用 ”高斯核函数“ 对附近点赋予更高权重

原理：
读入数据，将数据特征x、特征标签y存储在矩阵x、y中
利用高斯核构造一个权重矩阵 W，对预测点附近的点施加权重
验证 X^TWX 矩阵是否可逆
使用最小二乘法求得 回归系数 w 的最佳估计
"""

import os
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties

import numpy as np

#### 1.加载数据
def loadData(filename):
    fr = open(filename)
    dataArr = []
    labelArr= []
    for line in fr.readlines():
        line = line.split('\t')
        dataArr.append( [float(i) for i in line[0:len(line)-1]] )
        labelArr.append( float(line[-1]) )
    return dataArr, labelArr

#### 2. 计算样本权重    
def calWeights(testPoint, xMat, yMat, k=1.0):
    '''
    计算样本权重
    '''
    m = np.shape(xMat)[0]#样本个数
    weights = np.mat(np.eye(m))#单位矩阵
    
    for j in range(m):
        diffMat = testPoint - xMat[j,:]
        weights[j,j] = np.exp(diffMat * diffMat.T / (-2.0 * k ** 2))
    return weights
    
#### 3.使用局部加权计算回归系数w
def lwlr(testPoint, xArr, yArr, k = 1.0):
    """
    函数说明:使用局部加权线性回归计算回归系数w
    Parameters:
        testPoint - 测试样本点
        xArr - x数据集
        yArr - y数据集
        k - 高斯核的k,自定义参数
    Returns:
        ws - 回归系数
    """
    
    xMat = np.mat(xArr)
    yMat =np.mat(yArr).T
    
    weights = calWeights(testPoint, xMat, yMat, k)
    #判断xTWx行列式是否为0，判断矩阵逆是否存在
    xTWx = xMat.T * weights * xMat
    if np.linalg.det(xTWx) == 0.0:
        print('xTWx 矩阵为奇异矩阵，不能求逆')
        return
    ws = xTWx.I * (xMat.T * weights * yMat) #存在逆矩阵 xTWx.I，计算参数
    #预测值
    yHat = testPoint * ws
    return yHat
    
    # 计算回归系数 xTWx.I 求逆矩阵
    return testPoint * ws #测试结果
    
##### 局部加权线性回归测试
def lwlrTest(testArr, xArr, yArr, k=1.0):  
    """
    函数说明:局部加权线性回归测试
    Parameters:
        testArr - 测试数据集
        xArr - x数据集
        yArr - y数据集
        k - 高斯核的k,自定义参数
    Returns:
        ws - 回归系数
    """
    m = np.shape(testArr)[0]                                            #计算测试数据集大小
    yHat = np.zeros(m)    
    for i in range(m):                                                    #对每个样本点进行预测
        yHat[i] = lwlr(testArr[i],xArr,yArr,k)
    return yHat

#### 4.绘制多条局部加权回归曲线
def plotlwlrRegression():
    """
    函数说明:绘制多条局部加权回归曲线
    Parameters:
        无
    Returns:
        无
    """
    font = FontProperties(fname=r"c:\windows\fonts\simsun.ttc", size=14)
    xArr, yArr = loadData('ex0.txt')                                    #加载数据集
    yHat_1 = lwlrTest(xArr, xArr, yArr, 1.0)                            #根据局部加权线性回归计算yHat
    yHat_2 = lwlrTest(xArr, xArr, yArr, 0.1)                            #根据局部加权线性回归计算yHat
    yHat_3 = lwlrTest(xArr, xArr, yArr, 0.003)                            #根据局部加权线性回归计算yHat
    xMat = np.mat(xArr)                                                    #创建xMat矩阵
    yMat = np.mat(yArr)                                                    #创建yMat矩阵
    srtInd = xMat[:, 1].argsort(0)                                        #排序，返回索引值
    xSort = xMat[srtInd][:,0,:]
    fig, axs = plt.subplots(nrows=3, ncols=1,sharex=False, sharey=False, figsize=(10,8))                                        
    axs[0].plot(xSort[:, 1], yHat_1[srtInd], c = 'red')                        #绘制回归曲线
    axs[1].plot(xSort[:, 1], yHat_2[srtInd], c = 'red')                        #绘制回归曲线
    axs[2].plot(xSort[:, 1], yHat_3[srtInd], c = 'red')                        #绘制回归曲线
    axs[0].scatter(xMat[:,1].flatten().A[0], yMat.flatten().A[0], s = 20, c = 'blue', alpha = .5)                #绘制样本点
    axs[1].scatter(xMat[:,1].flatten().A[0], yMat.flatten().A[0], s = 20, c = 'blue', alpha = .5)                #绘制样本点
    axs[2].scatter(xMat[:,1].flatten().A[0], yMat.flatten().A[0], s = 20, c = 'blue', alpha = .5)                #绘制样本点
    #设置标题,x轴label,y轴label
    axs0_title_text = axs[0].set_title(u'局部加权回归曲线,k=1.0',FontProperties=font)
    axs1_title_text = axs[1].set_title(u'局部加权回归曲线,k=0.01',FontProperties=font)
    axs2_title_text = axs[2].set_title(u'局部加权回归曲线,k=0.003',FontProperties=font)
    plt.setp(axs0_title_text, size=8, weight='bold', color='red')  
    plt.setp(axs1_title_text, size=8, weight='bold', color='red')  
    plt.setp(axs2_title_text, size=8, weight='bold', color='red')  
    plt.xlabel('X')
    plt.show()
    
#### 绘制不同K下的样本权重图    
def plotSamplesWeigths():
    xArr, yArr = loadData('ex0.txt')
    xMat = np.mat(xArr)                                                    #创建xMat矩阵
    yMat = np.mat(yArr).T                                                    #创建yMat矩阵
    srtInd = xMat[:, 1].argsort(0)                                        #排序，返回索引值
    xSort = xMat[srtInd][:,0,:] #升序排序后数据，用于绘图
    yScort = yMat[srtInd]
    testPoint = np.mat([[1,0.5]])
    
    weights_1 = calWeights(testPoint, xSort, yScort, 1.0)                            #计算样本权重
    weights_2 = calWeights(testPoint, xSort, yScort, 0.1)                            #根据局部加权线性回归计算yHat
    weights_3 = calWeights(testPoint, xSort, yScort, 0.003)                            #根据局部加权线性回归计算yHat
    weights_1 = weights_1.diagonal()[0].T#提取对角线元素
    weights_2 = weights_2.diagonal()[0].T
    weights_3 = weights_3.diagonal()[0].T
    
    font = FontProperties(fname=r"c:\windows\fonts\simsun.ttc", size=14)

    fig, axs = plt.subplots(nrows=3, ncols=1,sharex=False, sharey=False, figsize=(10,8))                                        
    axs[0].plot(xSort[:, 1], weights_1, c = 'red')                        #绘制权重图
    axs[1].plot(xSort[:, 1], weights_2, c = 'red')                        #绘制权重图
    axs[2].plot(xSort[:, 1], weights_3, c = 'red')                        #绘制权重图
    #设置标题,x轴label,y轴label
    axs0_title_text = axs[0].set_title(u'x=0.5 样本权重图,k=1.0',FontProperties=font)
    axs1_title_text = axs[1].set_title(u'x=0.5样本权重图,k=0.1',FontProperties=font)
    axs2_title_text = axs[2].set_title(u'x=0.5样本权重图,k=0.003',FontProperties=font)

    plt.setp(axs0_title_text, size=8, weight='bold', color='red')  
    plt.setp(axs1_title_text, size=8, weight='bold', color='red')  
    plt.setp(axs2_title_text, size=8, weight='bold', color='red')  

    plt.xlabel('X')
    plt.show()
  
#### 计算均方差
def rssError(yArr,yHatArr):
    '''
    Desc:
        返回真实值与预测值误差大小
    Args：
        yArr：样本的真实值
        yHatArr：样本的预测值
    Returns:
        一个数字，代表误差
    '''
    return ((yArr-yHatArr)**2).sum()
    
#if __name__ == '__main__':
#    os.chdir(r'C:\Users\Administrator\Desktop\ML\8.LinearRegression')
#    #绘制多条局部加权回归曲线
#    plotlwlrRegression()
#    #参数 k 与权重的关系。
#    plotSamplesWeigths()
#    
    