# -*- coding: utf-8 -*-
"""
Created on Sat May 11 14:28:45 2019

@author: Administrator
"""
import os
import numpy as np 
from bs4 import BeautifulSoup
import sys
sys.path.append(r'C:\Users\Administrator\Desktop\ML\8.LinearRegression')
import LinearReg as standReg 
import LWLR as lwlr
import RidgeRegression as l2

def scrapePage(retX, retY, inFile, yr, numPce, origPrc):
    """
    函数说明:从页面读取数据，生成retX和retY列表
    Parameters:
        retX - 数据X
        retY - 数据Y
        inFile - HTML文件
        yr - 年份
        numPce - 乐高部件数目
        origPrc - 原价
    Returns:
        无
    Website:
        https://www.cuijiahua.com/
    Modify:
        2017-12-03
    """
    # 打开并读取HTML文件
    with open(inFile, encoding='utf-8') as f:
        html = f.read()
    soup = BeautifulSoup(html)
    i = 1
    # 根据HTML页面结构进行解析
    currentRow = soup.find_all('table', r = "%d" % i)
    while(len(currentRow) != 0):
        currentRow = soup.find_all('table', r = "%d" % i)
        title = currentRow[0].find_all('a')[1].text
        lwrTitle = title.lower()
        # 查找是否有全新标签
        if (lwrTitle.find('new') > -1) or (lwrTitle.find('nisb') > -1):
            newFlag = 1.0
        else:
            newFlag = 0.0
        # 查找是否已经标志出售，我们只收集已出售的数据
        soldUnicde = currentRow[0].find_all('td')[3].find_all('span')
        if len(soldUnicde) == 0:
            print("商品 #%d 没有出售" % i)
        else:
            # 解析页面获取当前价格
            soldPrice = currentRow[0].find_all('td')[4]
            priceStr = soldPrice.text
            priceStr = priceStr.replace('$','')
            priceStr = priceStr.replace(',','')
            if len(soldPrice) > 1:
                priceStr = priceStr.replace('Free shipping', '')
            sellingPrice = float(priceStr)
            # 去掉不完整的套装价格
            if  sellingPrice > origPrc * 0.5:
                print("%d\t%d\t%d\t%f\t%f" % (yr, numPce, newFlag, origPrc, sellingPrice))
                retX.append([yr, numPce, newFlag, origPrc])
                retY.append(sellingPrice)
        i += 1
        currentRow = soup.find_all('table', r = "%d" % i)
         
def setDataCollect(retX, retY):
    """
    函数说明:依次读取六种乐高套装的数据，并生成数据矩阵
    Parameters:
        无
    Returns:
        无
    Website:
        https://www.cuijiahua.com/
    Modify:
        2017-12-03
    """
    scrapePage(retX, retY, './lego/lego8288.html', 2006, 800, 49.99)                #2006年的乐高8288,部件数目800,原价49.99
    scrapePage(retX, retY, './lego/lego10030.html', 2002, 3096, 269.99)                #2002年的乐高10030,部件数目3096,原价269.99
    scrapePage(retX, retY, './lego/lego10179.html', 2007, 5195, 499.99)                #2007年的乐高10179,部件数目5195,原价499.99
    scrapePage(retX, retY, './lego/lego10181.html', 2007, 3428, 199.99)                #2007年的乐高10181,部件数目3428,原价199.99
    scrapePage(retX, retY, './lego/lego10189.html', 2008, 5922, 299.99)                #2008年的乐高10189,部件数目5922,原价299.99
    scrapePage(retX, retY, './lego/lego10196.html', 2009, 3263, 249.99)                #2009年的乐高10196,部件数目3263,原价249.99
    
def rssError(yArr,yHatArr):
    """
    函数说明:计算平方误差
    Parameters:
        yArr - 预测值
        yHatArr - 真实值
    Returns:
        
    Website:
        http://www.cuijiahua.com/
    Modify:
        2017-11-23
    """
    return ((yArr-yHatArr)**2).sum()    

 
def crossVvalidation(xArr, yArr, numVal = 10, lambdaNum = 30):
    """
    函数说明:交叉验证岭回归
    Parameters:
        xArr - x数据集
        yArr - y数据集
        numVal - 交叉验证次数
    Returns:
        wMat - 回归系数矩阵
    """
    xArr, yArr = lgX,lgY
    m = len(yArr)            #统计样本个数
    indexList = list(range(m)) #生成索引值列表
    errorMat= np.zeros((numVal,lambdaNum)) #在30个lambda下的numVal次交叉验证
    for i in range(numVal):
        trainX = [] ; trainY = []
        testX = []; testY = []
        np.random.shuffle(indexList)  #打乱顺序
        for j in range(m):#训练集和测试集比例为9:1
            if j < m * 0.9:
                trainX.append(xArr[indexList[j]])
                trainY.append(yArr[indexList[j]])
            else :
                testX.append(xArr[indexList[j]])
                testY.append(yArr[indexList[j]])
        #训练岭回归模型
        wMat = l2.ridgeTest(trainX,trainY)# #获得30个不同lambda下的岭回归系数
        #测试集进行标准化处理
        matTestX = np.mat(testX)
        matTrainX = np.mat(trainX)
        meanTrain = np.mean(matTrainX)
        varTrain = np.var(matTrainX)
        matTestX = (matTestX - meanTrain) / varTrain #测试集标准化
        #遍历30个人回归系数，进行测试集测试
        for k in range(len(wMat)):#遍历所有的岭回归系数
            yEst = matTestX * np.mat(wMat[k,:]).T + np.mean(trainY)                        #根据ws预测y值
            errorMat[i, k] = rssError(yEst.T.A, np.array(testY))                            #统计误差
    #errorMat：表示30个lambda下的多个误差值，
    meanErrors = np.mean(errorMat,0)                                                    #计算不同lambda的平均误差
    minMean = float(min(meanErrors))                                                    #找到最小误差
    bestWeights = wMat[np.nonzero(meanErrors == minMean)]                                #找到最佳回归系数
    xMat = np.mat(xArr); yMat = np.mat(yArr).T
    meanX = np.mean(xMat,0); varX = np.var(xMat,0)
    unReg = bestWeights / varX                                                            #数据经过标准化，因此需要还原
    print('%f%+f*年份%+f*部件数量%+f*是否为全新%+f*原价' % ((-1 * np.sum(np.multiply(meanX,unReg)) + np.mean(yMat)), unReg[0,0], unReg[0,1], unReg[0,2], unReg[0,3]))    
 
    
if __name__ == '__main__':
    os.chdir(r'C:\Users\Administrator\Desktop\ML\8.LinearRegression')
    lgX = []
    lgY = []
    setDataCollect(lgX, lgY)
    #对lgX添加常数项列
    data_num, feature_num = np.shape(lgX)
    lgX1 = np.mat(np.ones( (data_num,feature_num + 1) ))
    lgX1[:,1:lgX1.shape[1]] = np.mat(lgX)
    
    #简单线性回归
    #1.计算系数
    stand_ws = standReg.standRegres(lgX1,lgY)
    #训练模型的均方差为：
    stand_pred = standReg.predictData( stand_ws, lgX1)
    stand_regErr = standReg.rssError(lgY,stand_pred)
    print('简单线性回归的 lego 二手交易价预测方程为：')
    print('二手价 = %f%+f*年份%+f*部件数量%+f*是否为全新%+f*原价' % (stand_ws[0],stand_ws[1],stand_ws[2],stand_ws[3],stand_ws[4]))  
    #发现套件里的部件数量越多，售价反而降低了，不合理
    print('简单线性回归均方差为：',stand_regErr)
    
    #利用岭回归,并进行交叉验证来进行预测
    crossVvalidation(lgX, lgY,  10)
    print('岭回归的均方差：', )