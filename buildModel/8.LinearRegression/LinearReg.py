# -*- coding: utf-8 -*-
"""
线性回归
"""
import os
import matplotlib.pyplot as plt
import numpy as np

#### 1.加载数据
def loadData(filename):
    fr = open(filename)
    dataArr = []
    labelArr= []
    for line in fr.readlines():
        line = line.split('\t')
        dataArr.append( [float(i) for i in line[0:2]] )
        labelArr.append( float(line[-1]) )
    return dataArr, labelArr

#### 2.可视化数据
def plotData(filename):
    
    dataArr, labelArr = loadData("ex0.txt")
    xcord = [i[1] for i in dataArr]
    ycord = labelArr
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(xcord, ycord, c='blue', alpha=0.5)
    plt.title('plot dataset')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()
    
####   3.求解回归系数
def standRegres(xArr,yArr):
    """
    函数说明:计算回归系数w
    公式为： 
    Parameters:
        xArr - x数据集
        yArr - y数据集
    Returns:
        ws - 回归系数
    Website:
        https://www.cuijiahua.com/
    Modify:
        2017-11-12
    """
    xMat = np.mat(xArr)
    yMat = np.mat(yArr).T
    xTx = xMat.T * xMat                            #根据文中推导的公示计算回归系数
    if np.linalg.det(xTx) == 0.0: #矩阵行列式求解：np.linalg.det(xTx)
        print("矩阵为奇异矩阵,不能求逆")
        return None
    w = xTx.I * (xMat.T*yMat)
    return w

#### 4. 由回归系数，计算预测值
def predictData(ws, xArr):
    xMat = np.mat(xArr)                                                    #创建xMat矩阵
    yHat = xMat * ws                                                     #预测值 计算对应的y值
    return yHat
    
#### 5. 绘制曲线图
def plotRegression(xArr, yArr, ws):
    """
    函数说明:绘制回归曲线和数据点
    Parameters:
        无
    Returns:
        无
    """
    
    xMat = np.mat(xArr)                                                    #创建xMat矩阵
    yMat = np.mat(yArr)                                                    #创建yMat矩阵
    
    xCopy = xMat.copy()                                                    #深拷贝xMat矩阵
    xCopy.sort(0)                                                        #升序排序
    yHat = predictData(ws, xCopy)                                                     #预测值 计算对应的y值
    
    fig = plt.figure()
    ax = fig.add_subplot(111)                                            #添加subplot
    ax.plot(xCopy[:, 1], yHat, c = 'red')                                #绘制回归曲线
    ax.scatter(xMat[:,1].tolist(), yMat.tolist(), s = 20, c = 'blue',alpha = .5)                #绘制样本点  xMat[:,1].flatten().A[0]
    plt.title('DataSet')                                                #绘制title
    plt.xlabel('X')
    plt.show()

#### 5.回归模型评估：皮尔逊积矩相关系数,又称“相关系数”
def  modelAssessment(yMat,predMat):
    return np.corrcoef(yMat,predMat)
      
def rssError(yArr,predMat):
    err = 0.0
    for i in range(len(yArr)):
        err += (yArr[i] - predMat[i,0]) ** 2
    return err
    
if __name__ == '__main__':
    os.chdir(r'C:\Users\Administrator\Desktop\ML\8.LinearRegression')
    #加载数据
    xArr,yArr = loadData('ex0.txt')
    
    #可视化数据
    plotData('ex0.txt')
    
    #求解回归系数
    ws = standRegres(xArr,yArr)
    print('回归系数为：\n',ws)#第一个为常数项，第二个为特征对应的系数
    
    #预测数据
    predMat = predictData(ws, xArr)
    
    #绘制回归曲线图和数据点
    plotRegression(xArr, yArr, ws)
    
    #评估回归模型--皮尔逊相关系数
    print(modelAssessment( predMat.T, np.mat(yArr)))#转成行矩阵，并计算
    ''' [[1.         0.98647356]
        [0.98647356 1.        ]]
    可以看到，对角线上的数据是1.0，因为yMat和自己的匹配是完美的，而YHat和yMat的相关系数为0.98'''