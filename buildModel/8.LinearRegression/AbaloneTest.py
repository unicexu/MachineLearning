# -*- coding: utf-8 -*-
"""
线性回归项目案例：
预测鲍鱼年龄

项目描述：
我们有一份来自 UCI 的数据集合的数据，记录了鲍鱼（一种介壳类水生动物）的年龄。鲍鱼年龄可以从鲍鱼壳的层数推算得到。
工作流程：
收集数据: 采用任意方法收集数据
准备数据: 回归需要数值型数据，标称型数据将被转换成二值型数据
分析数据: 绘出数据的可视化二维图将有助于对数据做出理解和分析，在采用缩减法求得新回归系数之后，可以将新拟合线绘在图上作为对比
训练算法: 找到回归系数
测试算法: 使用 rssError()函数 计算预测误差的大小，来分析模型的效果
使用算法: 使用回归，可以在给定输入的时候预测出一个数值，这是对分类方法的提升，因为这样可以预测连续型数据而不仅仅是离散的类别标签
"""

import sys
sys.path.append(r'C:\Users\Administrator\Desktop\ML\8.LinearRegression')
import LWLR as lwlr #导入自定义模块
import LinearReg as standRegres #导入自定义模块

import os

if  __name__ == '__main__':
    #设置工作路径
    os.chdir(r'C:\Users\Administrator\Desktop\ML\8.LinearRegression')
    #导入鲍鱼年龄数据
    xArr,yArr = lwlr.loadData('abalone.txt')
    
    # 使用不同的核进行预测
    oldyHat01 = lwlr.lwlrTest(xArr[0:99], xArr[0:99], yArr[0:99], 0.1)
    oldyHat1 = lwlr.lwlrTest(xArr[0:99], xArr[0:99], yArr[0:99], 1)
    oldyHat10 = lwlr.lwlrTest(xArr[0:99], xArr[0:99], yArr[0:99], 10)   
    
    print("不同的核预测值与训练数据集上的真实值之间的误差大小")
    print("k=0.1误差:" , lwlr.rssError(yArr[0:99], oldyHat01.T)  )
    print( "k=1 误差:" , lwlr.rssError(yArr[0:99], oldyHat1.T)   )
    print( "k=10 误差:" , lwlr.rssError(yArr[0:99], oldyHat10.T) )
    
    #计算不同核函数拟合的曲线在新数据上的误差
    newyHat01 = lwlr.lwlrTest(xArr[100:199], xArr[0:99], yArr[0:99], 0.1)
    newyHat1 = lwlr.lwlrTest(xArr[100:199], xArr[0:99], yArr[0:99], 1)
    newyHat10 = lwlr.lwlrTest(xArr[100:199], xArr[0:99], yArr[0:99], 10)
    print("测试集中，局部加权线性回归对比：") 
    print ("k=0.1 误差:" , lwlr.rssError(yArr[100:199], newyHat01.T))
    print ("k=1 误差:" , lwlr.rssError(yArr[100:199], newyHat1.T))
    print ("k=10 误差 :" , lwlr.rssError(yArr[100:199], newyHat10.T))
    
    #######简单的线性回归下的误差情况
    ws = standRegres.standRegres(xArr[0:99],yArr[0:99]) #计算回归系数
    train_yHat = standRegres.predictData(ws, xArr[0:99]) #训练集预测结果
    test_yHat = standRegres.predictData(ws, xArr[100:199]) #测试集结果
    train_err = standRegres.rssError(yArr[0:99], train_yHat)
    test_err = standRegres.rssError(yArr[100:199], test_yHat)
    print('简单线性回归训练集和测试集拟合情况比较：')
    print('简单线性回归下的训练集误差：',train_err)
    print('简单线性回归下测试集误差：',test_err)
# https://cuijiahua.com/blog/2017/11/ml_11_regression_1.html
    
    
    
    