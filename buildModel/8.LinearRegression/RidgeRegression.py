# -*- coding: utf-8 -*-
"""
https://github.com/apachecn/AiLearning/blob/master/docs/ml/8.%E5%9B%9E%E5%BD%92.md
https://arxiv.org/pdf/1509.09169.pdf
http://statweb.stanford.edu/~tibs/sta305files/Rudyregularization.pdf

岭回归（ridge regression）：数据的特征比样本点还多应该怎么办？很显然，此时我们不能再使用上文的方法进行计算了，
因为矩阵X不是满秩矩阵，非满秩矩阵在求逆时会出现问题。为了解决这个问题，统计学家引入岭回归（ridge regression）的概念:即我们所说的L2正则线性回归

lasso法

前向逐步回归
"""
from matplotlib.font_manager import FontProperties
import matplotlib.pyplot as plt
import numpy as np

#### 1.加载数据
def loadData(filename):
    fr = open(filename)
    dataArr = []
    labelArr= []
    for line in fr.readlines():
        line = line.split('\t')
        dataArr.append( [float(i) for i in line[0:len(line)-1]] )
        labelArr.append( float(line[-1]) )
    return dataArr, labelArr

def regularize(xMat, yMat):
    """
    函数说明:数据标准化
    Parameters:
        xMat - x数据集
        yMat - y数据集
    Returns:
        inxMat - 标准化后的x数据集
        inyMat - 标准化后的y数据集
   
    """    
    inxMat = xMat.copy()                                                        #数据拷贝
    inyMat = yMat.copy()
    yMean = np.mean(yMat, 0)                                                    #行与行操作，求均值
    inyMat = yMat - yMean                                                        #数据减去均值
    inMeans = np.mean(inxMat, 0)                                                   #行与行操作，求均值
    inVar = np.var(inxMat, 0)                                                     #行与行操作，求方差
    inxMat = (inxMat - inMeans) / inVar                                            #数据减去均值除以方差实现标准化
    return inxMat, inyMat
 
def ridgeRegres(xMat, yMat, lam = 0.2):
    """
    函数说明:岭回归,计算回归系数
    Parameters:
        xMat - x数据集
        yMat - y数据集
        lam - 缩减系数
    Returns:
        ws - 回归系数
    """
    xTx = xMat.T * xMat
    denom = xTx + np.eye(np.shape(xMat)[1]) * lam
    if np.linalg.det(denom) == 0.0:
        print("矩阵为奇异矩阵,不能转置")
        return
    ws = denom.I * (xMat.T * yMat)
    return ws

def ridgeTest(xArr,yArr):
    '''
        Desc：
            函数 ridgeTest() 用于在一组 λ 上测试结果
        Args：
            xArr：样本数据的特征，即 feature
            yArr：样本数据的类别标签，即真实数据
        Returns：
            wMat：将所有的回归系数输出到一个矩阵并返回
    '''
    xMat = np.mat(xArr)
    yMat = np.mat(yArr).T
    # 计算Y的均值
    yMean = np.mean(yMat,0)
    # Y的所有的特征减去均值
    yMat = yMat - yMean
    # 标准化 x，计算 xMat 平均值
    xMeans = np.mean(xMat,0)
    # 然后计算 X的方差
    xVar = np.var(xMat,0)
    # 所有特征都减去各自的均值并除以方差
    xMat = (xMat - xMeans)/xVar
    # 可以在 30 个不同的 lambda 下调用 ridgeRegres() 函数。
    numTestPts = 30
    # 创建30 * m 的全部数据为0 的矩阵
    wMat = np.zeros((numTestPts,np.shape(xMat)[1]))
    for i in range(numTestPts):
        # exp() 返回 e^x 
        ws = ridgeRegres(xMat,yMat,np.exp(i-10))
        wMat[i,:] = ws.T
    return wMat



#test for ridgeRegression

def plotwMat():
    """
    函数说明:绘制岭回归系数矩阵
    Website:
        http://www.cuijiahua.com/
    Modify:
        2017-11-20
    """
    font = FontProperties(fname=r"c:\windows\fonts\simsun.ttc", size=14)
    abX, abY = loadData('abalone.txt')
    redgeWeights = ridgeTest(abX, abY)
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    x = range(-10,20)#绘制log(lambada)与回归系数的关系，这里的log(lambada)在-10,19之间，因为上面定义lambada为：np.exp(i-10)
    xMat = np.mat([x for i in range(8)]).T
    ax.plot(xMat,redgeWeights) 
   
    ax_title_text = ax.set_title(u'log(lambada)与回归系数的关系', FontProperties = font)
    ax_xlabel_text = ax.set_xlabel(u'log(lambada)', FontProperties = font)
    ax_ylabel_text = ax.set_ylabel(u'回归系数', FontProperties = font)
    plt.setp(ax_title_text, size = 20, weight = 'bold', color = 'red')
    plt.setp(ax_xlabel_text, size = 10, weight = 'bold', color = 'black')
    plt.setp(ax_ylabel_text, size = 10, weight = 'bold', color = 'black')
    plt.show()
  

if __name__ == '__main__':
    #log(lambada)与回归系数的关系
    plotwMat()
    '''
    该图绘制出了回归系数与log(lambada)的关系，最左边，lambada最小，得到所有系数的原始值，最右边，lambada最大，系数全部缩减为 0，
    在中间部分的某些值可以取得最好的预测效果
    '''
    
