import numpy as np
from numpy import *

def loadExData():
    """
    # 推荐引擎示例矩阵
    return[[4, 4, 0, 2, 2],
           [4, 0, 0, 3, 3],
           [4, 0, 0, 1, 1],
           [1, 1, 1, 2, 0],
           [2, 2, 2, 0, 0],
           [1, 1, 1, 0, 0],
           [5, 5, 5, 0, 0]]
    """
    return [[4, 4, 0, 2, 2],
            [4, 0, 0, 3, 3],
            [4, 0, 0, 1, 1],
            [1, 1, 1, 2, 0],
            [2, 2, 2, 0, 0],
            [1, 1, 1, 0, 0],
            [5, 5, 5, 0, 0]]
    # 原矩阵
    # return[[1, 1, 1, 0, 0],
    #        [2, 2, 2, 0, 0],
    #        [1, 1, 1, 0, 0],
    #        [5, 5, 5, 0, 0],
    #        [1, 1, 0, 2, 2],
    #        [0, 0, 0, 3, 3],
    #        [0, 0, 0, 1, 1]]


    """
    菜肴矩阵
    行：代表人
    列：代表菜肴名词
    值：代表人对菜肴的评分，0表示未评分
    """
    # return[[2, 0, 0, 4, 4, 0, 0, 0, 0, 0, 0],
    #        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5],
    #        [0, 0, 0, 0, 0, 0, 0, 1, 0, 4, 0],
    #        [3, 3, 4, 0, 3, 0, 0, 2, 2, 0, 0],
    #        [5, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0],
    #        [0, 0, 0, 0, 0, 0, 5, 0, 0, 5, 0],
    #        [4, 0, 4, 0, 0, 0, 0, 0, 0, 0, 5],
    #        [0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 4],
    #        [0, 0, 0, 0, 0, 0, 5, 0, 0, 5, 0],
    #        [0, 0, 0, 3, 0, 0, 0, 0, 4, 5, 0],
    #        [1, 1, 2, 1, 1, 2, 1, 0, 4, 5, 0]]

    # # 原矩阵
    # return[[0, -1.6, 0.6],
    #        [0, 1.2, 0.8],
    #        [0, 0, 0],
    #        [0, 0, 0]]
#计算SVD
def cal_svd(datMat):
    U, Sigma, VT = np.linalg.svd(datMat)
    # print(U)
    # print(VT)
    # print(Sigma)
    return U, Sigma, VT

#相似度计算
def euclidSim(inA, inB):
    '''
    相似度 = 1/（1+距离）
    :param inA:
    :param inB:
    :return:
    '''
    return 1.0 / (1.0 + np.linalg.norm(inA - inB))

def pearsSim(inA, inB):
    """
    皮尔逊距离 = 0.5 + 0.5*corrcoef()
    :param inA:
    :param inB:
    :return:
    """

    if len(inA) < 3:
        return 1
    return 0.5 + 0.5 * np.corrcoef(inA, inB, rowvar=0)[0][1]

def cosSim(inA, inB):
    '''
    inA, inB 均为列向量
    cos_theta = inA.T * inB/norm(A)*norm(B)
    由于余弦值范围为[-1,1]，将其转化为[0,1]
    cos_theta_change = 0.5 + 0.5*cos_theta

    :param inA:
    :param inB:
    :return:
    '''
    num = float(inA.T * inB)
    denom = np.linalg.norm(inA) * np.linalg.norm(inB)
    cos_theta = num / denom
    return 0.5 + 0.5 * cos_theta

#基于物品相似度推荐系统
# 基于物品相似度的推荐引擎
#计算用户对物品的评分估计值：在用户已经评分过的物品中根据物品的相似度进行综合评分：
#   # logical_and 计算x1和x2元素的真值。逻辑与
def standEst(dataMat, user, simMeas, item, xformedItems=None):
    """standEst(计算某用户未评分物品中，以对该物品和其他物品评分的用户的物品相似度，然后进行综合评分)

    Args:
        dataMat         训练数据集
        user            用户编号
        simMeas         相似度计算方法
        item            未评分的物品编号
    Returns:
        ratSimTotal/simTotal     评分（0～5之间的值）
    """
    # 得到数据集中的物品数目,列代表物品
    n = shape(dataMat)[1]
    # 初始化两个评分值
    simTotal = 0.0
    ratSimTotal = 0.0

    # 遍历行中的每个物品（对用户评过分的物品进行遍历，并将它与其他物品进行比较）
    for j in range(n):
        userRating = dataMat[user, j]
        # 如果某个物品的评分值为0，则跳过这个物品
        if userRating == 0:
            continue
        # 寻找两个用户都评级的物品
        # 变量 overLap 给出的是两个物品当中已经被评分的那个元素的索引ID
        overLap = nonzero(logical_and(dataMat[:, item].A > 0, dataMat[:, j].A > 0))[0]
        # 如果相似度为0，则两着没有任何重合元素，终止本次循环
        if len(overLap) == 0:
            similarity = 0
        # 如果存在重合的物品，则基于这些重合物重新计算相似度。
        else:
            similarity = simMeas(dataMat[overLap, item], dataMat[overLap, j])
        # 相似度会不断累加，每次计算时还考虑相似度和当前用户评分的乘积
        # similarity  用户相似度，   userRating 用户评分
        simTotal += similarity
        ratSimTotal += similarity * userRating
    if simTotal == 0:
        return 0
    # 通过除以所有的评分总和，对上述相似度评分的乘积进行归一化，使得最后评分在0~5之间，这些评分用来对预测值进行排序
    else:
        return ratSimTotal/simTotal

#计算需要转换为几维特征
def svd_analyse(datMat):
    import matplotlib.pyplot as plt
    from matplotlib.font_manager import FontProperties
    font_set = FontProperties(fname='/Library/Fonts/Songti.ttc', size=15)

    U, Sigma, VT = cal_svd(datMat)
    Sigma2 = Sigma ** 2
    Sigma2_total = sum(Sigma2)#计算总能量
    sort_index = argsort(Sigma2)[::-1] #降序排序

    total_ratio = 0
    total_ratio_list = []
    for i in sort_index:
        total_ratio += float(Sigma2[sort_index[i]])/Sigma2_total
        total_ratio_list.append(total_ratio)

    plt.figure(111)
    plt.title('能量贡献率', fontproperties=font_set)
    x = range(1, min(21,len(Sigma2)+1))
    plt.plot(x, total_ratio_list, marker='o', mec='r', mfc='w')

    plt.margins(0)
    plt.subplots_adjust(bottom=0.10)
    plt.xlabel('feature number')  # X轴标签
    plt.ylabel("ratio %")  # Y轴标签
    plt.show()
    return U, Sigma, VT

def dat_svd_transform(datMat):
    '''
    数据利用SVD进行降维处理
    '''
    #分析数据需要降到几维
    U, Sigma, VT = svd_analyse(datMat)
    # 这里需要计算需要几个特征，利用奇异值累计占比在90%的元素数
    lowD = input('映射到几维空间，奇异值累计占比在90%的元素数：')
    lowD = int(lowD.strip())
    SigLowD = mat(eye(lowD) * Sigma[: lowD])
    # 利用U矩阵将物品转换到低维空间中，构建转换后的物品(物品+4个主要的特征)
    xformedItems = datMat.T * U[:, :lowD] * SigLowD.I  # 行代表商品，列代表评价特征

    return xformedItems

def svdEst(datMat, user, simMeas, item, xformedItems=None):
     '''

     :param datMat: 原始评分数据
     :param user:
     :param simMeas:
     :param item:
     :param xformedItems: 降维后的新特征数据
     :return:
     '''
     n = shape(datMat)[1] #商品数目
     simTotal = 0.0
     ratSimTotal = 0.0

     # 对于给定的用户，for循环在用户对应行的元素上进行遍历
     # 这和standEst()函数中的for循环的目的一样，只不过这里的相似度计算时在低维空间下进行的。
     for j in range(n):
         userRating = datMat[user, j]
         if userRating == 0 or j == item:
             continue
         #之前列代表商品，行代表用户的评价即特征，现在行代表商品，列代表评价特征，
         #而计算相似度是按照列向量计算的，将数据再转成列向量
         similarity = simMeas(xformedItems[item, :].T, xformedItems[j, :].T)
         # for 循环中加入了一条print语句，以便了解相似度计算的进展情况。如果觉得累赘，可以去掉
         # print('the item  %d to be estimated and %d similarity is: %f' % (item, j, similarity))
         # 对相似度不断累加求和
         simTotal += similarity
         # 对相似度及对应评分值的乘积求和
         ratSimTotal += similarity * userRating
     if simTotal == 0:
         return 0
     else:
         # 计算估计评分
         return ratSimTotal / simTotal
#产生N个推荐结果
#基于物品相似度对user未评分的商品进行评分，并返回评分较高的前N个商品
def recommand(datMat, user, N=3, simMeas=cosSim, estMethod = standEst):
    unratedItems = nonzero(datMat[user, :].A == 0)[1]#用户未评分的商品的下标
    if len(unratedItems) == 0:
        return "you rated everything"#所有商品用户均评价过

    xformedItems = None
    if estMethod == svdEst:
        #如果是利用svd推荐系统，先将数据进行降维处理
        #分析数据需要降到几维, 然后进行转换
         xformedItems = dat_svd_transform(datMat)

    itemScores = []  # 所有待评价商品的评分估计
    for item in unratedItems:
        estimatedScore = estMethod(datMat, user, simMeas, item, xformedItems) #对item商品的评价进行估计
        # print(' item %d 评估分： %f'%(item, estimatedScore))
        itemScores.append((item, estimatedScore))
    #对评分排序,降序，并获取前N项
    topN = sorted(itemScores, key=lambda jj: jj[1], reverse=True)[:N]
    return topN

def testSim():
    #测试相似度
    datMat = loadExData()
    datMat = np.mat(datMat)
    U, Sigma, VT = cal_svd(datMat)
    #欧式距离
    print(euclidSim(datMat[:, 0], datMat[:, 1]))

    #皮尔逊相似度
    pearsSim(datMat[:, 0], datMat[:, 4])

    #余弦相似度
    cosSim(datMat[:, 0], datMat[:, 4])

if __name__ == '__main__':
    datMat = loadExData()
    datMat = np.mat(datMat)

    print('推荐系统：', recommand(datMat, user=2))#行代表用户 列代表商品

    print('svd推荐系统： ', recommand(datMat, user=2, estMethod=svdEst))

