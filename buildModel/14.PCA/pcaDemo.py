import pca
import numpy as np
from numpy import *
def replaceNanMean():
    '''
    将数据中缺失项替换为对应均值

    :return:
    '''

    datArr = pca.loadData('../data/13.PCA/secom.data', ' ')
    numFeat = shape(datArr)[1]  # 特征数
    for i in range(numFeat):
        meanVal = mean(datArr[nonzero(~isnan(datArr[:, i]))[0], i])
        datArr[nonzero(isnan(datArr[:, i]))[0], i] = meanVal  # 将所有NaN值替换为对应均值
    return datArr

#方差占比情况分析
def analyse_data(dataMat, topNfeat = 20):
    import matplotlib.pyplot as plt
    from matplotlib.font_manager import FontProperties

    font_set = FontProperties(fname='/Library/Fonts/Songti.ttc', size=15)

    meanVals = mean(dataMat, axis=0)
    meanRemoved = dataMat-meanVals
    covMat = cov(meanRemoved, rowvar=0)
    eigVals, eigVects = linalg.eig(mat(covMat))

    # print('特征值 = ', eigVals ) # 发现特征值很多都是0
    nonzero_len = len(nonzero(eigVals)[0])  # 非0个数
    zero_len = len(eigVals) - nonzero_len  # 0的个数
    print('特征值为0的占比 = %f' % (zero_len / len(eigVals)))

    eigValInd = argsort(abs(eigVals))
    eigValInd = eigValInd[:-(topNfeat+1):-1]

    cov_all_score = float(sum(abs(eigVals)))
    sum_cov_score = 0
    feat_var_ratio = np.zeros((topNfeat, 1))
    var_ratio = np.zeros((topNfeat, 1))

    for i in range(0, len(eigValInd)):
        line_cov_score = abs(float(eigVals[eigValInd[i]]))
        sum_cov_score += line_cov_score
        '''
        我们发现其中有超过20%的特征值都是0。
        这就意味着这些特征都是其他特征的副本，也就是说，它们可以通过其他特征来表示，而本身并没有提供额外的信息。
        最前面15个值的数量级大于10^5，实际上那以后的值都变得非常小。
        这就相当于告诉我们只有部分重要特征，重要特征的数目也很快就会下降。
        最后，我们可能会注意到有一些小的负值，他们主要源自数值误差应该四舍五入成0.
        '''
        feat_var_ratio[i] = line_cov_score / cov_all_score * 100
        var_ratio[i] = sum_cov_score / cov_all_score * 100
        # print('主成分：%s, 方差占比：%s%%, 累积方差占比：%s%%' % (format(i+1, '2.0f'), format(line_cov_score/cov_all_score*100, '4.2f'), format(sum_cov_score/cov_all_score*100, '4.1f')))

    # print('top %d 特征方差占比：\n'%(topNfeat), feat_var_ratio)
    # print('top %d 特征方差累计占比：\n'%(topNfeat), var_ratio)

    plt.figure(111)
    plt.title('方差百分比', fontproperties=font_set)
    x = range(1, topNfeat+1)
    p1 = plt.plot(x, feat_var_ratio, marker='o', mec='r', mfc='w', label='var-ratio')#特征方差占比
    p2 = plt.plot(x, var_ratio, marker='*', ms=10, label='sum-var-ratio')#方差累计占比
    plt.legend(scatterpoints=1, loc='upper right', ncol=3, fontsize=8)  # 让图例生效

    plt.margins(0)
    plt.subplots_adjust(bottom=0.10)
    plt.xlabel('feature number')  # X轴标签
    plt.ylabel("ratio %")  # Y轴标签
    # plt.title("A simple plot") #标题
    plt.show()


def pca_plot3(lowDDataMat3):
    from matplotlib import pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    import random

    fig = plt.figure()
    ax = Axes3D(fig)

    sequence_containing_x_vals = lowDDataMat3[:, 0]
    sequence_containing_y_vals = lowDDataMat3[:, 1]
    sequence_containing_z_vals = lowDDataMat3[:, 2]

    random.shuffle(sequence_containing_x_vals)
    random.shuffle(sequence_containing_y_vals)
    random.shuffle(sequence_containing_z_vals)

    ax.scatter(sequence_containing_x_vals, sequence_containing_y_vals, sequence_containing_z_vals)
    plt.show()
    plt.savefig('fig.png', bbox_inches='tight')

if __name__ == '__main__':
    datArr = replaceNanMean()#将所有NaN值替换为对应均值
    #分析数据
    analyse_data(datArr)
    #选取3个主成分
    lowDDataMat3, reconMat3 = pca.pca(datArr, 3)
    #绘制主成分
    pca_plot3(lowDDataMat3)