# 机器学习
## 一、特征工程 
见 featureEngineering目录
### 1. 特征工程是什么？
### 2. 数据预处理　
    2.1 无量纲化　　　　
      2.1.1 标准化　　　　
      2.1.2 区间缩放法
      2.1.3 标准化与归一化的区别　　     
    2.2 对定量特征二值化　
    2.3 对定性特征哑编码
    2.4 缺失值处理
    2.5 异常值检测 
        2.5.1 箱线图
        2.5.2 Isolation Tree 孤立森林
        2.5.3 Local outlier factor (LOF)局部异常系数
        2.5.4 Fitting eliptic envelope椭圆模型模拟　　
    2.6 数据变换    
### 3. 特征分箱
    3.1. 无监督分箱
      3.1.1 等频分箱
      3.1.2 等距分箱
      3.1.3 聚类分箱
    3.2.有监督分箱
      3.2.1 决策树分箱(可以是决策树，gbd)
            treeDiscretization.py
      3.2.2 卡方分箱
            chiMerge.py
      3.2.3 基于熵的分箱mdlp
            mdlp.py/ mdlp_api.py
      cut_bins.py
### 4. 特征选择　　
    4.1 Filter　　　　
      4.1.1 方差选择法　　　　
      4.1.2 相关系数法　　　　
      4.1.3 卡方检验　　　　
      4.1.4 互信息法　　
    4.2 Wrapper　　　　
      4.2.1 递归特征消除法　　
    4.3 Embedded　　　　
      4.3.1 基于惩罚项的特征选择法　　　　
      4.3.2 基于树模型的特征选择法
### 5. 特征组合、衍生特征
    5.1 FM 
        FM: factorization machine 因子分解机，主要解决数据稀疏数据的特征组合问题
    5.2 FFM 
        FFM：field-aware factorization machine,引入field概念，FFM每一个
        特征归属于一个特定的 和Field
    5.3 GBDT+LR
### 6. 降维　　
    6.1 主成分分析法（PCA）　　
    6.2 线性判别分析法（LDA）
     
## 二、建模
### 1. 交叉验证(对样本不平衡问题也适用)
cross_validation_util.py
### 2. 样本不平衡问题 （imbalanceData下）
#### 2.1 采样方法
     2.1.1 过采样overSampling
		1) randomOverSampler:随机过采样
		2) Synthetic合成过采样
		3) SMOTE
		4) SMOTE变体
			a) border-line SMOTE
			b) SMOTE+Tomek
		5) ADASYN
     2.1.2 欠采样underSampling
		1) prototype generation:原型生成
			ClusterCentroids:每一个类别的样本都会用K-Means算法的中心点来进行合成,
		2) prototype selection:原型选择
			a) RandomUnderSample：随机欠采样
			b) NearMiss：添加了启发式的规则来选择样本，利用距离远近剔除多数类样本的一类方法
				NearMiss-1:选择离N个近邻的负样本的平均距离最小的正样本
				NearMiss-2:选择离N个负样本最远的平均距离最小的正样本
				NearMiss-3是一个两段式的算法. 首先, 对于每一个负样本, 保留它们的M个近邻样本; 接着, 那些到N个近邻样本平均距离最大的正样本将被选择.
			c) Cleaning under-sampling techniques:基于某种规则，对重叠的数据进行清洗, 主要有：
				 Tomek’s links：TOMEK
				 Edited Nearest Neighbours（ENN）
	2.1.3 过采样和欠采样结合
		1) SMOTE + TOMEK
		2) SMOTE + ENN
#### 2.2 Ensemble集成学习方法
	2.2.1 EasyEnsamble
	2.2.2 BalancedCascade
    
### 3. 建立模型
    3.1 有监督学习
        3.1.1 分类问题
        3.1.2 回归问题
    3.2 无监督学习
        3.2.1 K-means
            K-means, K-means++, 二分k-means
        3.2.2 Apriori 关联分析
        3.2.3 SVD简化数据
            SVD简化数据
            基于SVD的CF推荐系统
        3.2.4 PCA主成分分析
        3.2.5 LDA线性判别分析
     
## 三、 模型优化
    1. 集成模型ensemble
        1.1 bagging
            RandomForest
        1.2 boosting
            1. AdaBoost
            2. GBDT
            3. XGB
        1.3.blending 和 Stacking         
        
## 四、模型评估