'''
bagging算法之随机森林 sklearn 实现
'''
print(__doc__)
import numpy as np
np.random.seed(0)
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from sklearn.calibration import calibration_curve

#加载数据
def loadData():
    X, y = datasets.make_classification(n_samples=100000, n_features=20,
                                        n_informative=2, n_redundant=2)
    return  X, y

def rf_demo1():
    #
    X, y = loadData()

    train_samples = 100  # Samples used for training the models

    X_train = X[:train_samples]
    X_test = X[train_samples:]
    y_train = y[:train_samples]
    y_test = y[train_samples:]

    # Create classifiers
    lr = LogisticRegression(solver='lbfgs')
    gnb = GaussianNB()
    svc = LinearSVC(C=1.0)

    rfc = RandomForestClassifier(n_estimators=100, max_depth=2, random_state=0)

    # #############################################################################
    # Plot calibration plots

    plt.figure(figsize=(10, 10))
    ax1 = plt.subplot2grid((3, 1), (0, 0), rowspan=2)
    ax2 = plt.subplot2grid((3, 1), (2, 0))

    ax1.plot([0, 1], [0, 1], "k:", label="Perfectly calibrated")
    for clf, name in [(lr, 'Logistic'),
                      (gnb, 'Naive Bayes'),
                      (svc, 'Support Vector Classification'),
                      (rfc, 'Random Forest')]:
        clf.fit(X_train, y_train)
        if hasattr(clf, "predict_proba"):
            prob_pos = clf.predict_proba(X_test)[:, 1]
        else:  # use decision function
            prob_pos = clf.decision_function(X_test)
            prob_pos = \
                (prob_pos - prob_pos.min()) / (prob_pos.max() - prob_pos.min())
        fraction_of_positives, mean_predicted_value = \
            calibration_curve(y_test, prob_pos, n_bins=10)

        ax1.plot(mean_predicted_value, fraction_of_positives, "s-",
                 label="%s" % (name,))

        ax2.hist(prob_pos, range=(0, 1), bins=10, label=name,
                 histtype="step", lw=2)

    ax1.set_ylabel("Fraction of positives")
    ax1.set_ylim([-0.05, 1.05])
    ax1.legend(loc="lower right")
    ax1.set_title('Calibration plots  (reliability curve)')

    ax2.set_xlabel("Mean predicted value")
    ax2.set_ylabel("Count")
    ax2.legend(loc="upper center", ncol=2)

    plt.tight_layout()
    plt.show()

def rf_demo2():
    '''
    sklearn.model_selection.cross_val_score进行k折交叉验证
    '''
    from sklearn.model_selection import cross_val_score
    from sklearn.ensemble import ExtraTreesClassifier
    from sklearn.tree import DecisionTreeClassifier

    X, y = loadData()

    #决策树
    dt_clf = DecisionTreeClassifier( max_depth=None, min_samples_split=2, random_state = 0)
    #随机森林
    rf_clf = RandomForestClassifier(n_estimators=10, max_depth=None, min_samples_split=2, random_state = 0)
    #
    et_clf = ExtraTreesClassifier(n_estimators=10, max_depth=None, min_samples_split=2, random_state=0)

    for clf, title in zip([dt_clf, rf_clf, et_clf], ['dtc', 'rfc', 'etc']):
        print("模型-'%s'结果" % title)
        scores = cross_val_score(clf, X, y, cv=5)  # 5折集交叉验证
        print('交叉验证得分：', scores)
        print('平均得分： ', scores.mean())

#无放回k重抽样
def cross_validation_split_2(dataset, k_folds=5):
    '''Kfold可以将数据随机分成多个训练集和测试集

    :returns
       test_kfolds： 生成的 k_folds 训练集
    '''

    from sklearn.model_selection import KFold
    kf = KFold(n_splits=k_folds)

    train_kfolds = list()
    test_kfolds = list()
    for train_index, test_index in kf.split(dataset):
        train, test = dataset[train_index], dataset[test_index]
        train_kfolds.append(train)
        test_kfolds.append(test)
    return train_kfolds, test_kfolds

def model_eval(pred, real):
    #分类模型评估
    from sklearn import metrics
    accuracy = metrics.accuracy_score(real, pred)
    precision = metrics.precision_score(real, pred)
    recall = metrics.recall_score(real, pred)
    f1 = metrics.f1_score(real, pred)
    # 混淆矩阵
    confusion_matrix = metrics.confusion_matrix(real, pred)
    # print('accuracy:', accuracy)
    # print('precision:', precision)
    # print('recall:', recall)
    # print('f1:', f1)
    # print("confusion_matrix: ", confusion_matrix)

    # 数据保存为字典
    eval= {'accuracy': accuracy, 'precision': precision, 'recall': recall, 'f1': f1,
            'confusion_matrix': confusion_matrix}
    return eval

def rf_demo3():
    X, y = loadData()
    (m, n) = X.shape
    dataset = list()
    for i in  range(m):
        tmp = np.hstack((X[i, :], y[i]))
        dataset.append(list(tmp))

    k_folds = 5
    train_kfolds, test_kfolds = cross_validation_split_2(np.array(dataset), k_folds)
    features = train_kfolds[0].shape[1] - 1

    train_evals = list()
    test_evals = list()
    for i in range(k_folds):
        train = train_kfolds[i]
        test = test_kfolds[i]
        train_X, train_y = train[:, 0:features], train[:, -1]
        test_X, test_y = test[:, 0:features], test[:, -1]

        rfc = RandomForestClassifier(n_estimators=10, max_depth=None, min_samples_split=2, random_state=0)
        rfc.fit(train_X, train_y)
        train_pred = rfc.predict(train_X)
        train_eval = model_eval(train_pred, train_y)
        train_evals.append(train_eval)

        test_pred = rfc.predict(test_X)
        test_eval = model_eval(test_pred, test_y)
        test_evals.append(test_eval)

    train_acc = list()#交叉验证中训练集的准确率
    test_acc = list()#验证集的准确率

    for i in range(k_folds):
        train_acc.append(train_evals[i]['accuracy'])
        test_acc.append(test_evals[i]['accuracy'])
    print('train-set accuracy :\n', train_acc, ',\n mean: ', np.mean(train_acc))
    print('dev-set accuracy :\n', test_acc, ',\n mean: ', np.mean(test_acc))


if __name__ == '__main':
    rf_demo3()


    