"""
1. 采样方法：
    1)随机采样算法：包括随机过采样(Oversampling)和随机降采样(Undersampling)等;
    2)合成采样算法：其中包括SMOTE，Border-line-SMOTE, ADASYN, SMOTE+Tomek, OSS等;
    3)基于聚类的采样算法：例如CBO等。

2. 代价敏感型方法：
    代价敏感型(Cost-Sensitive)方法通过改变在模型训练过程中，
    不同类别样本分类错误时候的代价值来获得具有一定数据倾向性的分类模型
    常见的代价敏感型方法有MetaCost, AdaCost, Adaptive Scaling等

3. 集成学习方法
    集成学习(Ensemble)是一种通过多分类器融合来获取更强分类性能的方法，将采样方法和代价敏感方法与之结合
    可以获得多种基于集成学习的数据不平衡下的学习算法，按照集成学习方法的不同，可以分为以下三类：
    1)基于Bagging的方法。主要思想是将采样方法应用到Bagging算法的抽样过程中，具体操作时一般是首先从不均衡数据中抽取多个均衡子数据集，分别训练多个分类器，然后做Bagging模型集成。主要算法包括OverBagging, UnderBagging, UnderOverBagging, IIVotes等。
    2)基于Boosting的方法。主要思想是将Boosting方法与采样方法或代价敏感型方法结合使用，因此又可以分为2小类：
        a)代价敏感+Boosting: Boosting过程中更改类别权重，迭代时更关注少数类样本。主要算法包括AdaCost, CSB1, CSB2, RareBoost, AdaC1等。
        b)采样方法+Boosting: Boosting过程中更改样本分布，迭代时更关注少数类样本。主要算法包括SMOTEBoost, MSMOTEBoost, RUSBoost, DataBoost-IM等。
    3)Hybrid方法，即将基于Bagging的方法和基于Boosting的方法结合使用，主要算法包括EsayEnsemble和BalanceCascade等。

4. 主动学习方法
    前面三小节的方法都是在数据集已经标注完成的基础上，通过优化数据处理方法或优化机器学习算法来提升模型在不平衡数据集上的分类性能。本小节所述的主动学习(Active Learning)方法则更关注于数据标注过程，将数据标注与模型训练迭代进行，每轮迭代在完成模型训练之后，通过一定的算法查询最有用的未标记样本，并交由专家进行标记，然后用查询到的样本重新训练分类模型来提高模型的较精确度。

5. 其他
"""
import imblearn
imblearn.__version__
imblearn.__path__
from collections import Counter

def loadData():
    from sklearn.datasets import make_classification
    X, y = make_classification(n_samples=5000, n_features=2, n_informative=2,
                               n_redundant=0, n_repeated=0, n_classes=3,
                               n_clusters_per_class=1,
                               weights=[0.01, 0.05, 0.94],
                               class_sep=0.8, random_state=0)
    return  X, y

#1. ------------------------------过采样 overSampling------------------------------

## 1.1 随机过采样
def random_overSampling(X, y):
    from imblearn.over_sampling import RandomOverSampler
    ros = RandomOverSampler(random_state=0)
    X_resampled, y_resampled = ros.fit_sample(X, y)
    print('采样前各类数据情况：', Counter(y))
    print("随机过采样后的各类数据情况：", Counter(y_resampled))
    return X_resampled, y_resampled

## 1.2 从随机采样 —> 合成采样 SMOTE,ADASYN

##1.2.1 SMOTE
def smote(X, y):
    """
    SMTO: Synthetic Minority Oversampling Technique
    对于少数类样本a, 随机选择一个k-近邻的样本b（可能为少数实例，也可能为多数实例）, 然后从a与b的连线上随机选取一个点c作为新的少数类样本;
    一定程度上可以弥补随机过采样的缺陷，但在合成过程中可能会生成大量无效或者低价值样本

    参数说明：
    sampling_strategy = ‘auto’,
    random_state = None, ## 随机器设定
    k_neighbors = 5, ## 用相近的 5 个样本（中的一个）生成正样本
    m_neighbors = 10, ## 当使用 kind={'borderline1', 'borderline2', 'svm'}
    out_step = ‘0.5’, ## 当使用kind = 'svm'
    kind = 'regular', ## 随机选取少数类的样本
     – borderline1： 最近邻中的随机样本b与该少数类样本a来自于不同的类
     – borderline2： 随机样本b可以是属于任何一个类的样本;
     – svm：使用支持向量机分类器产生支持向量然后再生成新的少数类样本
    svm_estimator = SVC(), ## svm 分类器的选取
    n_jobs = 1, ## 使用的例程数，为-1时使用全部CPU
    ratio=None

    :return:
    """
    from imblearn.over_sampling import SMOTE
    X_resampled_smote, y_resampled_smote = SMOTE(random_state=0).fit_sample(X, y)
    print('采样前各类数据情况：', Counter(y))
    print("过采样后的各类数据情况：", Counter(y_resampled))
    return X_resampled, y_resampled

##1.2.2 ADASYN
def adasyn(X, y):
    """
    adasyn:adaptive synthetic自适应综合过采样
    对少数实例a,在k近邻中随机选取一个少数实例，然后从a与b的连线上随机选取一个点c作为新的少数类样本;
    :param X:
    :param y:
    :return:
    """
    from imblearn.over_sampling import ADASYN
    X_resampled_adasyn, y_resampled_adasyn = ADASYN().fit_sample(X, y)
    print('采样前各类数据情况：', Counter(y))
    print("过采样后的各类数据情况：", Counter(y_resampled))
    return X_resampled_adasyn, y_resampled_adasyn

## 1.2.3 SMOTE变体：Border-line-SMOTE
def border_line_smote(X, y):
    """
    相对于基本的SMOTE算法, 关注的是所有的少数类样本, 这些情况可能会导致产生次优的决策函数,
    因此SMOTE就产生了一些变体: 这些方法关注在最优化决策函数边界的一些少数类样本, 然后在最近邻类的相反方向生成样本.
    Borderline-SMOTE 算法尝试着在训练过程中尽量地去学习边界特征，认为边界样本在分类中比远离边界的样本更容易被错分，
    一个类的边界样本携带了更多的信息，对分类器分类性能的好坏起到了决定性的作用。
    因此，在 Borderline-SMOTE算法中，加入了识别边界样本的过程：若一个少数类样本的 m 近邻中，半数以上为多数类样本，则认
    为这个样本为容易被错分的危险样本；否则为安全样本。在危险样本与其 k 近邻之间合成新样本，完成对边界少数类样本的过采样，以此加强少数类的
    决策边界，以获得好的分类结果*
    :param X:
    :param y:
    :return:
    """
    from imblearn.over_sampling import SMOTE
    X_resampled, y_resampled = SMOTE(kind='borderline1').fit_sample(X, y)
    print('采样前各类数据情况：', Counter(y))
    print("过采样后的各类数据情况：", sorted(Counter(y_resampled).items()))
    return X_resampled, y_resampled

# 2. ------------------------------欠采样  underSampling 待补充------------------------------
# 2.1 随机欠采样
def random_underSampling(X, y):
    from imblearn.under_sampling import RandomUnderSampler
    ros = RandomUnderSampler(random_state=0)
    X_resampled, y_resampled = ros.fit_sample(X, y)
    print('采样前各类数据情况：', Counter(y))
    print("随机过采样后的各类数据情况：", Counter(y_resampled))
    return X_resampled, y_resampled
if __name__ == '__mian__':
    X, y = loadData()
    Counter(y) #样本数据是不平衡的

    #1. 过采样--overSampling

    ## 1.1 随机过采样
    X_resampled, y_resampled = random_overSampling(X, y)
    #1.2 合成方法
    ##1.2.1 SMOTE
    X_resampled_smote, y_resampled_smote = smote(X,y)
    ##1.2.2 ADASYN
    X_resampled_adasyn, y_resampled_adasyn = adasyn(X, y)
    ## 1.2.3 SMOTE变体：Border-line-SMOTE
    X_resampled_bl_smote, y_resampled_bl_smote = border_line_smote(X, y)


    # 2.欠采样
    ### 2.1 ClusterCenter：
    X_resampled, y_resampled = random_underSampling(X, y)

    ### 2.2




