import pandas as pd
import matplotlib.pyplot as plt
from sklearn import neighbors
import numpy as np
seed = 10
np.random.seed(seed)
from collections import Counter
class MinMaxNormalization:
    """
    Min-Max Normalization. Was using in conjunction of ADASYN to test results
    data: Data to be normalized
    axis: 0 is by columns, 1 is by rows
    returns: Normalized data
    """
    def __init__(self, data, axis=0):
        self.row_min = np.min(data, axis=axis)
        self.row_max = np.max(data, axis=axis)
        self.denominator = abs(self.row_max - self.row_min)
        # Fix divide by zero, replace value with 1 because these usually happen for boolean columns
        for index, value in enumerate(self.denominator):
            if value == 0:
                 self.denominator[index] = 1
    def __call__(self, data):
        return np.divide((data - self.row_min), self.denominator)

def adasyn(X, y, beta, K, threshold=1):
    """
    Adaptively generating minority data samples according to their distributions.
    More synthetic data is generated for minority class samples that are harder to learn.
    Harder to learn data is defined as positive examples with not many examples for in their respective neighbourhood.
    Inputs
    -----
    X: Input features, X, sorted by the minority examples on top. Minority example should also be labeled as 1
    y: Labels, with minority example labeled as 1
    beta: Degree of imbalance desired. A 1 means the positive and negative examples are perfectly balanced.
    K: Amount of neighbours to look at
    threshold: Amount of imbalance rebalance required for algorithm
    Variables
    -----
    xi: Minority example
    xzi: A minority example inside the neighbourhood of xi
    ms: Amount of data in minority class
    ml: Amount of data in majority class
    clf: k-NN classifier model
    d: Ratio of minority : majority
    beta: Degree of imbalance desired
    G: Amount of data to generate
    Ri: Ratio of majority data / neighbourhood size. Larger ratio means the neighbourhood is harder to learn,
    thus generating more data.
    Minority_per_xi: All the minority data's index by neighbourhood
    Rhat_i: Normalized Ri, where sum = 1
    Gi: Amount of data to generate per neighbourhood (indexed by neighbourhoods corresponding to xi)
    Returns
    -----
    syn_data: New synthetic minority data created
    """
    X_ms = X[y == 1, :] #负样本 少数类样本
    X_ml = X[y == 0, :] #正样本 多数类样本
    ms = int(sum(y)) #这里是以二分类为例，1为负样本，表示少数类的个数
    ml = len(y) - ms #多数类实例个数
    clf = neighbors.KNeighborsClassifier()
    clf.fit(X, y)

    #step1
    #计算少数与多数实例的比率,如果d低于某个阈值，则初始化算法
    d = np.divide(ms, ml)
    if d > threshold:
        return print("The data set is not imbalanced enough.")

    # Step 2a
    # 计算要生成的合成少数数据的总数。G=(ml-ms)*beta
    # G为生成少数数据的总数，beta是ADASYN之后所需的少数数据的比率
    #若beta=1,则表示ADASYN之后的完美平衡数据集，
    #若beta>1，生成更多的少数类数据
    G = (ml - ms) * beta
    # Step 2b
    # the ratio ri = #majority/k, #majority表示 k近邻 中多数实例个数
    #找到每个少数实例的k-近邻，并计算rᵢ值。在此步骤之后，每个少数实例应与不同的邻域相关联.
    # rᵢ值表示每个特定邻域中多数类的主导地位。较高的rᵢ邻域包含更多的多数类示例，并且更难学习
    Ri = []
    Minority_per_xi = []
    for i in range(ms):
        xi = X_ms[i, :].reshape(1, -1)
        #返回k个邻居对应的下标
        neighbours = clf.kneighbors(xi, n_neighbors=K, return_distance=False)[0]
        # Skip classifying itself as one of its own neighbours
        # neighbours = neighbours[1:]
        # 计算k个邻居中属于多数类的个数
        count = 0
        for value in neighbours:
            if y[value] == 0:
                count += 1
        Ri.append(count / K)
        # Find all the minority examples
        minority = []
        for value in neighbours:
            if y[value] == 1:
                minority.append(value)
        Minority_per_xi.append(minority)

    # Step 2c
    # 归一化ri值
    Rhat_i = []
    for ri in Ri:
        rhat_i = ri / sum(Ri)
        Rhat_i.append(rhat_i)
    assert (sum(Rhat_i) > 0.99)
    # Step 2d
    #计算每个邻域生成的合成实例的数量。
    Gi = []
    for rhat_i in Rhat_i:
        gi = round(rhat_i * G)
        Gi.append(int(gi))

    #Step 2e
    #为每个邻域生成Sᵢ数据。
    # 首先，少数数据xi的邻域为例。然后，随机选择该邻域中的另一个少数实例，xzᵢ。
    # 可以使用以下公式计算新的合成实例：si = xi + (xzᵢ-xi)*lambda
    syn_data = []
    for i in range(ms):
        xi = X[i, :].reshape(1, -1)
        gi =  Gi[i] #生成少数类实例数量
        minority_per_xi = Minority_per_xi[i]#k近邻中为少数类的对应下标

        if (gi > 0) and (len(minority_per_xi)>0): #如果k近邻中包含少数类实例,且生成数>0
            index = np.random.choice(minority_per_xi) #随机选取k近邻中一个少数类
            xzi = X[index, :].reshape(1, -1)
            si = xi + (xzi - xi) * np.random.uniform(0, 1)
            syn_data.append(si)

    # Test the new generated data
    test = []
    for values in syn_data:
        a = clf.predict(values)
        test.append(a)
    print("Using the old classifier, {} out of {} would be classified as minority.".format(np.sum(test), len(syn_data)))
    # Build the data matrix
    data = []
    for values in syn_data:
        if len(values) == 0:
            continue
        for i in range(len(values)):
            data.append(values[i])
    print("{} amount of minority class samples generated".format(len(data)))
    # Concatenate the positive labels with the newly made data
    labels = np.ones([len(data), 1])
    data = np.concatenate([labels, data], axis=1)
    #
    org_data = np.concatenate([y.reshape(-1, 1), X], axis=1)
    new_data = np.concatenate([data, org_data])
    return new_data, Minority_per_xi, Ri


if __name__ == "__main__":
    path = '/Users/unice-xu/PycharmProjects/ml/FeatureEngineering/'
    df = pd.read_csv(path + 'imbalancedData/fk_data.csv')
    df.reset_index(drop=True, inplace=True)
    X = df.drop(df.columns[0], axis=1).values
    X = X.astype('float32')
    y = df.iloc[:, 0].values
    Syn_data, neighbourhoods, Ri = adasyn(X, y, beta=0.8, K=8, threshold=1)#neighbourhoods,k近邻中为少数类的下标，Ri:k近邻中多数类的占比

    print('总数据量：', Syn_data.shape)
    print('各分类数据比例', Counter(Syn_data[:,0]))
    np.savetxt(path + 'data/syn_10_data_plc_1500.csv', Syn_data, delimiter=',')

