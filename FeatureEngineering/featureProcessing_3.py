"""
特征工程3 --- 数据降维
"""
import numpy as np
import pandas as pd
from numpy import *
#1. 加载数据
def loadData():
    from sklearn.datasets import load_iris
    #导入数据集
    iris = load_iris()
    #特征矩阵
    data = iris.data
    #目标变量
    target = iris.target
    return data, target
#PCA
def pca(data, topN=2):
    from sklearn.decomposition import PCA

    # 计算每一列的均值
    meanVals = mean(data, axis=0)
    # 每个向量同时都减去 均值
    meanRemoved = data - meanVals
    # cov协方差=[(x1-x均值)*(y1-y均值)+(x2-x均值)*(y2-y均值)+...+(xn-x均值)*(yn-y均值)+]/(n-1)
    '''
    方差：（一维）度量两个随机变量关系的统计量
    协方差： （二维）度量各个维度偏离其均值的程度
    协方差矩阵：（多维）度量各个维度偏离其均值的程度

    当 cov(X, Y)>0时，表明X与Y正相关；(X越大，Y也越大；X越小Y，也越小。这种情况，我们称为“正相关”。)
    当 cov(X, Y)<0时，表明X与Y负相关；
    当 cov(X, Y)=0时，表明X与Y不相关。
    '''
    covMat = cov(meanRemoved, rowvar=0)

    # eigVals为特征值， eigVects为特征向量
    eigVals, eigVects = linalg.eig(mat(covMat))
    print('特征值：', eigVals)
    #
    # # 特征值的逆序就可以得到topNfeat个最大的特征向量
    # eigValInd = argsort(np.abs(eigVals))
    # # -1表示倒序，返回topN的特征值[-1 到 -(topNfeat+1) 但是不包括-(topNfeat+1)本身的倒叙]
    # eigValInd = eigValInd[:-(topN + 1):-1]
    # # 重组 eigVects 最大到最小
    # redEigVects = eigVects[:, eigValInd]
    # # 将数据转换到新空间
    # lowDDataMat = meanRemoved * redEigVects
    # reconMat = (lowDDataMat * redEigVects.T) + meanVals

    # 主成分分析法，返回降维后的数据
    # 参数n_components为主成分数目
    data_pca = PCA(n_components=2).fit_transform(data)
    return data_pca

#LDA
def lda(data, target):
    from sklearn.decomposition import LatentDirichletAllocation
    # 线性判别分析法，返回降维后的数据
    # 参数n_components为降维后的维数
    data_lda = LatentDirichletAllocation(n_components=2).fit_transform(data, target)
    return data_lda

if __name__ == '__main__':
    data, target = loadData()
    data_pca = pca(data)
    print(data_pca[0, ])
    data_lda = lda(data, target)