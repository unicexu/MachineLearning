"""
计算信息熵
"""
from __future__ import division
__author__ = 'unice'

import pandas as pd
import numpy as np
from math import log

def entropy_numpy(data_classes, base=2):
    '''
    Computes the entropy of a set of labels (class instantiations)
    :param base: logarithm base for computation
    :param data_classes: Series with labels of examples in a dataset
    :return: value of entropy
    '''
    classes = np.unique(data_classes)
    N = len(data_classes)
    ent = 0.0  # initialize entropy

    #pi*log(pi) i=1,...k代表类别数
    for c in classes:
        partition = data_classes[data_classes == c]  # data with class = c
        proportion = len(partition) / N
        #update entropy
        ent -= proportion * log(proportion, base)

    return ent

def cut_point_information_gain_numpy(X, y, cut_point):

    """
    在给定的cut_point下，将数据分成两部分，计算信息增益
    :param X: 数据框对应列数据
    :param y: 标签数据
    :param cut_point: 切分数据的阈值点，<= cut_point为一部分，>cut_point为一部分
    :return: 返回切分前后的信息增益
    """
    entropy_full = entropy_numpy(y)  #数据未切分下的信息熵
    #split data at cut_point
    data_left_mask = (X <= cut_point) #<=cut_point数据
    data_right_mask = (X > cut_point) #>cut_point数据
    (N, N_left, N_right) = (len(X), data_left_mask.sum(), data_right_mask.sum())

    entropy_left = entropy_numpy(y[data_left_mask])
    entropy_right = entropy_numpy(y[data_right_mask])
    condition_entropy = N_left / N * entropy_left + N_right / N * entropy_right #条件熵
    gain = entropy_full - condition_entropy#计算信息增益
    # print('切分前信息熵：', entropy_full)
    # print('切分后信息熵：', condition_entropy)
    return gain

if __name__ == '__main__':
    iris = pd.read_csv('ml/FeatureEngineering/iris.csv', header=None)
    iris.columns = ['sepal_length', 'sepal_width', 'petal_length', 'petal_width', 'target']

    #初始时信息熵
    ent_0 = entropy_numpy(iris['target'])
    #计算信息增益
    gain = cut_point_information_gain_numpy(iris['sepal_length'], iris['target'], 4.7)