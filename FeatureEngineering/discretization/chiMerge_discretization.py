#ChiMerge 是监督的、自底向上的(即基于合并的)数据离散化方法。它依赖于卡方分析：具有最小卡方值的相邻区间合并在一起，直到满足确定的停止准则。
#ChiMerge算法包括2部分：1、初始化，2、自底向上合并，当满足停止条件的时候，区间合并停止。


import math
import itertools
import numpy as np
import pandas as pd

#将最小卡方值添加进新的数据框中
def merge_rows(chi2_df, feature):
    """
    合并区间 ：将最小的卡方值对应区间进行合并
    :param chi2_df:
    :param feature:
    :return:
    """

    chi2 = chi2_df.iloc[:-1, -1]#卡方值对应列的数据,最后一行数据无效的卡方值,去掉最后一行数据
    distinct_values = sorted(list(set(chi2)), reverse=False)#卡方值进行排序

    col_names = chi2_df.columns.tolist()
    updated_df = pd.DataFrame(columns=col_names)  #加入卡方值，生成新的数组
    
    updated_df_index = 0
    for index, row in chi2_df.iterrows():
        if index == 0:
            updated_df.loc[len(updated_df)] = chi2_df.loc[index]
            updated_df_index += 1
        else:
            if chi2_df.loc[index-1, 'chi2'] == distinct_values[0]: #将卡方值最小的进行合并
                for class_name in col_names[1:len(col_names)-1]:#每一类统计数据更新
                    updated_df.loc[updated_df_index-1, class_name] += chi2_df.loc[index, class_name] #最小值，合并
            else:#否则，不进行合并
                updated_df.loc[len(updated_df)] = chi2_df.loc[index]
                updated_df_index += 1
                
    updated_df['chi2'] = 0. #新的数据框卡方值初始化为0， 进行下一轮计算
    return updated_df

#计算卡方值
def calc_chi2(prep_chi2):
    """

    :param prep_chi2: 列代表类别统计
                      行代表区间统计
    :return: 卡方值
    """
    # prep_chi2 = np.array([[0,3,1],[0,7,1]])
    (intervels, classes) = prep_chi2.shape  #intervels为区间数量，classes 类别数
    n = float(prep_chi2.sum()) #总数
    row_total = prep_chi2.sum(axis=0)#按行统计求和，即每一类的总数
    col_total = prep_chi2.sum(axis=1)#按列求和，即每一区间的总数

    chi2 = 0
    #卡方计算公式
    for i in range(intervels):
        for j in range(classes):
            E_ij = col_total[i] * row_total[j] / n
            A_ij = prep_chi2[i, j]
            if E_ij == 0.:
                chi2 += 0.  #确保不存在NaN值
            else:
                chi2 += math.pow((A_ij - E_ij), 2) / float(E_ij)
    return chi2
    
#计算每一个类别的卡方值
def update_chi2_column(contingency_table, feature):
    col_name = contingency_table.columns.tolist()
    target_names = col_name[1:len(col_name)-1]
    for index, row in contingency_table.iterrows():
        if index != contingency_table.shape[0]-1:
            list1 = []
            list2 = []
            for target_name in target_names:
                list1.append(contingency_table.loc[index][target_name])
                list2.append(contingency_table.loc[index + 1][target_name])
            prep_chi2 = np.array([np.array(list1), np.array(list2)])

            c2 = calc_chi2(prep_chi2) # 计算相邻区间的卡方值

            contingency_table.loc[index, 'chi2'] = c2 #修改卡方值
    return contingency_table

#计算频次表
def create_contingency_table(df, feature, target):
    df2 = df.copy()
    distinct_values = sorted(list(set(df2[feature])), reverse=False) #特征数据去重，排序，作为分箱数据

    target_names = list(set(df[target]))
    col_names = list([[feature], target_names, ['chi2']])
    col_names = list(itertools.chain(*col_names))
    #[['sepal_length'], ['Iris-setosa', 'Iris-virginica', 'Iris-versicolor'], ['chi2']]
    #平铺开为['sepal_length', 'Iris-setosa', 'Iris-virginica', 'Iris-versicolor', 'chi2']

    my_contingency = pd.DataFrame(columns=col_names)
    my_contingency[feature] = distinct_values
    
    #就算每一分组中的个分类个数
    fea_tar_group = df2.groupby([feature, target]).size()

    for i in range(len(distinct_values)):
        target_count = []
        for j in target_names:
            try:
                A_ij = fea_tar_group.loc[distinct_values[i], j]
            except:
                A_ij = 0
            target_count.append(A_ij)

        target_count.insert(0, distinct_values[i])
        target_count.append(0)

        my_contingency.loc[i] = target_count
    return my_contingency

#ChiMerge
def chimerge(feature, data, target= 'target_class', max_interval=6):
    """

    :param feature: 分箱的特征列名
    :param data: 待分箱的原始数据，包含标签列（最后一列）的数据框
    :param target:标签列名
    :param max_interval: max_interval 最大分箱个数
    :return feature_cut：分箱后的特征
    :return cut_bins: 分箱分区间
    """

    df = data.sort_values(by=[feature], ascending=True).reset_index(drop=True) #按特征数据进行升序排序
    #传入频次表
    contingency_table = create_contingency_table(df, feature, target)

    #计算初始间隔值
    num_intervals = contingency_table.shape[0]

    #是否满足最大间隔，若大于最大分箱个数，按卡方值进行合并，将相似的相邻区间合并，直到满足最大分箱个数
    while num_intervals > max_interval: 
        #相邻列的卡方值
        chi2_df = update_chi2_column(contingency_table, feature)
        contingency_table = merge_rows(chi2_df, feature)#将最小的卡方值进行合并
        num_intervals = contingency_table.shape[0]

    #得出分箱对应的分段点
    print('The split points for '+feature+' are:')
    for index, row in contingency_table.iterrows():
        print(contingency_table.loc[index][feature])

    #输出分段区间
    print('The final intervals for '+feature+' are:')

    df_feature = np.array(df[feature])
    cuts = contingency_table[feature].tolist()
    cuts.append(max(df_feature)+0.001)

    feature_cut = pd.cut(df_feature, cuts, right=False)
    print(feature_cut.categories)
    cut_bins = feature_cut.categories #划分区间
    return feature_cut, cut_bins

if __name__=='__main__':
    iris = pd.read_csv('Chimerge-master/iris.csv', header=None)
    iris.columns = ['sepal_length', 'sepal_width',
                    'petal_length', 'petal_width', 'target_class']
    for feature in ['sepal_length', 'sepal_width', 'petal_length', 'petal_width']:
        feature_cut, cut_bins = chimerge(feature=feature, data=iris, target='target_class', max_interval=6)
