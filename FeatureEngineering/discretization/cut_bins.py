"""
特征工程--特征分箱处理

"""

from mdlp.discretization import MDLP
from sklearn.datasets import load_iris
import numpy as np
from sklearn.model_selection import train_test_split
import pandas as pd

#1. 无监督分箱
##1.1等频分箱

##1.2 等距分箱

##1.3聚类分箱

#2.有监督分箱
##2.1 决策树分箱
def tree_discretization():
    from treeDiscretization import optimal_binning_boundary
    dataset = load_iris()
    data = dataset.data
    target = dataset.target
    iris = pd.DataFrame(data)
    iris.columns = ['sepal_length', 'sepal_width', 'petal_length', 'petal_width']
    features = iris.columns
    y = np.array(target)
    for feature in features:
        x = np.array(iris[feature])
        boundary = optimal_binning_boundary(x, y)
        print("%s boundary: " % feature, boundary)


#2.2 卡方分箱discretization
def  chi2_discretization():

    from chiMerge import chiMerge#自定义模块
    dataset = load_iris()
    data = dataset.data
    target = dataset.target
    iris = pd.DataFrame(data)
    iris['target'] = target
    iris.columns = ['sepal_length', 'sepal_width','petal_length', 'petal_width', 'target_class']
    for feature in ['sepal_length', 'sepal_width', 'petal_length', 'petal_width']:
        feature_cut, cut_bins = chimerge(feature=feature, data=iris, target='target_class', max_interval=6)

#2.3 差熵分箱
def mdlp():
    """
    mdlp : https://github.com/hlin117/mdlp-discretization

    """
    dataset = datasets.load_iris()
    X, y = dataset['data'], dataset['target']
    feature_names, class_names = dataset['feature_names'], dataset['target_names']
    numeric_features = np.arange(X.shape[1])  # 这里特征都为数值型变量，需进行离散化处理

    # Split between training and test
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33)  # 切分训练集和测试集

    # Initialize discretizer object and fit to training data
    discretizer = MDLP()
    discretizer.fit(X_train, y_train)  # 离散化后，并进行了编码处理
    X_train__disc = discretizer.transform(X_train)
    print(X_train__disc)

    # apply same discretization to test set
    X_test__disc = discretizer.transform(X_test)
    print(X_test__disc)