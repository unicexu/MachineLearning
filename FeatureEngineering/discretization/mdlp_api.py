"""
mdlp : https://github.com/hlin117/mdlp-discretization

"""
from mdlp.discretization import MDLP
from sklearn import datasets
import numpy as np
from sklearn.model_selection import train_test_split
def demo():


    dataset = datasets.load_iris()
    X, y = dataset['data'], dataset['target']
    feature_names, class_names = dataset['feature_names'], dataset['target_names']
    numeric_features = np.arange(X.shape[1])  # 这里特征都为数值型变量，需进行离散化处理

    # Split between training and test
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33)  # 切分训练集和测试集

    # Initialize discretizer object and fit to training data
    discretizer = MDLP()
    discretizer.fit(X_train, y_train)  # 离散化后，并进行了编码处理
    X_train__disc = discretizer.transform(X_train)
    print(X_train__disc)

    # apply same discretization to test set
    X_test__disc = discretizer.transform(X_test)
    print(X_test__disc)
