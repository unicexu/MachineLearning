"""
特征工程 --- 1
"""
import numpy as np
import pandas as pd
from numpy import *
#1. 加载数据
def loadData():
    from sklearn.datasets import load_iris
    #导入数据集
    iris = load_iris()
    #特征矩阵
    data = iris.data
    #目标变量
    target = iris.target
    return data, target
#数据预处理
#2. 无量纲化 sklearn.preprocessing
def standardScaler(X):
    """
    2.1 标准化处理
    标准化后，其转换成标准正态分布
    每一特征数据(xi-mean(xi))/var(xi)
    """
    from sklearn.preprocessing import StandardScaler
    return StandardScaler().fit_transform(X)

"""
P.s. 区分标准化和归一化
    P.s.    
     归一化：
    １）把数据变成(０，１)或者（1,1）之间的小数。主要是为了数据处理方便提出来的，把数据映射到0～1范围之内处理，更加便捷快速。
    ２）把有量纲表达式变成无量纲表达式，便于不同单位或量级的指标能够进行比较和加权。
    归一化是一种简化计算的方式，即将有量纲的表达式，经过变换，化为无量纲的表达式，成为纯量。
    归一化方法：min-max归一化，l2归一化
    
    标准化：
    在机器学习中，我们可能要处理不同种类的资料，例如，音讯和图片上的像素值，这些资料可能是高维度的，
    资料标准化后会使每个特征中的数值平均变为0(将每个特征的值都减掉原始资料中该特征的平均)、标准差变为1，
    这个方法被广泛的使用在许多机器学习算法中(例如：支持向量机、逻辑回归和类神经网络)。z-score
"""
def minMaxScaler(X):
    """
    2.2.1 归一化处理之 min-max
    (x-min(x))/(max(x)-min(x))


    """
    from sklearn.preprocessing import MinMaxScaler
    return MinMaxScaler().fit_transform(X)

def normlizer(X):
    """
    2.2.2 归一化处理--l2归一化
    x/np.linalg.norm(x)
    """
    from sklearn.preprocessing import Normalizer
    return Normalizer().fit_transform(X)

#3.定量数据二值化
def binarizer(x, x_thredshld):
    """
    定量特征二值化的核心在于设定一个阈值，大于阈值的赋值为1，小于等于阈值的赋值为0

    :param x: 需二值化处理的数据
    :param thredshld: 指定阈值
    :return: 返回二值化后的数据
    """
    from sklearn.preprocessing import Binarizer
    return Binarizer(threshold=x_thredshld).fit_transform(x)

#4.对定性特征编码处理
def labelEncoder(X):
    """
    4.1 类别型输出变量进行编码处理, 可处理多分类数据
    eg.
    y = ['a','b','c','a']
    label_encoder = LabelEncoder()
    label_encoder = label_encoder.fit(y)
    label_encoded_y = label_encoder.transform(y)

    :param X:类别型变量
    :return: encoded_X: 编码后数据
    """
    from sklearn.preprocessing import LabelEncoder

    # demo:--------------------------------------------------------
    # X = [['a', 'b'], ['b', 'x'], ['q', 'a']]
    X = np.array(X)
    features = []
    for i in range(0, X.shape[1]):
        feature = LabelEncoder().fit_transform(X[:, i])
        features.append(feature)
    encoded_X = np.array(features)  # 行代表特征数，列代表样本个数
    encoded_X = encoded_X.reshape(X.shape[0], X.shape[1])  # encoded_X转置一下，列数代表特征数，行代表样本个数
    print("LabelEncoder编码后数据维度:", encoded_X.shape)
    return encoded_X

def onehotEncoder(X):
    """
    4.2 输入变量为分类变量,进行独热编码
    :param X: 分类变量，可多列分类特征
    :return: 编码后的数据
    """
    from sklearn.preprocessing import OneHotEncoder

    # X = [['a', 'b'], ['b', 'x'], ['q', 'a']]
    X = np.array(X)
    X = np.reshape(X, (len(X), -1))
    encoded_X = None
    for i in range(0, X.shape[1]):
        feature = X[:, i]
        feature = feature.reshape(X.shape[0], 1)  # 转置，1*m -----m*1

        onehot_encoder = OneHotEncoder(sparse=False)
        feature = onehot_encoder.fit_transform(feature)
        if encoded_X is None:
            encoded_X = feature
        else:
            encoded_X = np.concatenate((encoded_X, feature), axis=1)  # 数组拼接
    print("LabelEncoder编码后数据维度:", encoded_X.shape)

    return encoded_x

#5. 异常数据处理
#### 5.1 缺失值计算
def missingValuesFill(data, fill_type='mean'):
    """
    缺失值处理
    使用可用特征的均值来填补缺失值；
    使用特殊值来填补缺失值，如-1；
    忽略有缺失值的样本；
    使用相似样本的均值添补缺失值；
    使用另外的机器学习算法预测缺失值。

    缺失值填充：
    from sklearn.preprocessing import Imputer
    Imputer().fit_transform(）
    参数missing_value为缺失值的表示形式，默认为NaN
    参数strategy为缺失值填充方式，默认为mean（均值）,median, most_frequent
    由于：DeprecationWarning: Class Imputer is deprecated; Imputer was deprecated in version 0.20
        and will be removed in 0.22. Import impute.SimpleImputer from sklearn instead.
        warnings.warn(msg, category=DeprecationWarning)
    改成：from sklearn.impute import SimpleImputer
         SimpleImputer().fit_transform(）

    :param data:数据，此时格式为数据框df
           fill_type: 填充方式: mean（均值）,median, most_frequent
    :return: fill_data: 填充后数据，数据类型为dataframe
    """
    from sklearn.impute import SimpleImputer

    data_new = data.copy()

    n = data_new.shape[0]  # 样本个数
    null_num = data_new.isnull().sum().sort_values(ascending=False)
    null_rate = null_num / float(n)
    print('数据缺失比例:')
    print(null_rate)

    # 去除缺失值超过80%的特征
    remove = null_rate[null_rate > 0.8].index.tolist()
    # 去除缺失比例超过80%的指标
    if len(remove) > 0:
        data_new = data_new.drop(remove, 1)
        print("缺失比例超过80%的特征有：%d 个 " % len(remove))
        print("去除缺失比例超过80%的特征后，剩下的数据维度:  ",data_new.shape)

    # # 缺失比例<0.01的选择删除，看作是脏数据，或者进行数据填充
    # remove_lines_index = null_rate[(null_rate < 0.01) & (null_rate > 0)].index.tolist()
    # # 删除这些列为空的所在行
    # for index in remove_lines_index:
    #     data_new = data_new.drop(data_new[data_new[index].isnull()].index)
    # print('样本删除后维度: ', data_new.shape)

    #缺失值填充
    fill_data = SimpleImputer(strategy=fill_type).fit_transform(data_new)#数据类型为数组
    fill_data = pd.DataFrame(fill_data)
    fill_data.columns = data_new.columns
    return fill_data

#### 5.2 离群点检测
######5.2.1 箱线图 ：异离群点检测 并用边缘值对异常值进行赋值QL－1.5IQR或大于QU＋1.5IQR赋值
# ( 其他离群点检测方法：见OutlierDetection.py :孤立森林IsolationForest，局部异常系数LOF(local outlier factor),椭圆模型拟合（fitting an eliptic envelope  ）)
def outlier_detection(data):
    # 1)分位数，箱线图 ：异常值通常被定义为小于QL－1.5IQR或大于QU＋1.5IQR的值
    #其中QL下四分位数，QU上四分位数，IQR：四分位间距

    import matplotlib.pyplot as plt
    for col in data.columns:
        col_dat = data[col]
        #--------------------------
        #绘制箱线图，观察异常点
        plt.figure()
        plt.boxplot(col_dat, notch=True,  # 设置中位线处凹陷，（注意：下图看起来有点丑）
                    patch_artist=True,  # 设置用自定义颜色填充盒形图，默认白色填充
                    showmeans=True,  # 以点的形式显示均值
                    boxprops={"color": "black", "facecolor": "#F43D68"},  # 设置箱体属性，填充色and 边框色
                    flierprops={"marker": "o", "markerfacecolor": "#59EA3A", "color": "#59EA3A"},
                    # 设置异常值属性，点的形状、填充色和边框色
                    meanprops={"marker": "D", "markerfacecolor": "white"},  # 设置均值点的属性，点的形状、填充色
                    medianprops={"linestyle": "--", "color": "#FBFE00"})
        plt.xlabel(col)
        plt.show()
        # --------------------------
        # 计算分位数
        # Q1(25%):下四分位数
        Q1 = np.percentile(col_dat, 25)
        # Q3(75%):上四分位数
        Q3 = np.percentile(col_dat, 75)
        #(IQR)四分位间距
        IQR = Q3 - Q1
        # outlier step
        outlier_step = 1.5 * IQR
        lower_outlier = Q1 - outlier_step
        higher_outlier = Q3 + outlier_step
        #将异常点数据修改为对应边缘数据
        col_dat[col_dat < lower_outlier] = lower_outlier
        col_dat[col_dat > higher_outlier] = higher_outlier
        data[col] = col_dat
    return data

#### 5.3 重复值处理（这一步应该在最开始就应该数据去重）

#6. 数据变换
def data_transform(data):
    '''
    常见的数据变换有基于多项式的、基于指数函数的、基于对数函数的
    preproccessing库的PolynomialFeatures类对数据进行多项式转换
    :return:
    '''
    from sklearn.preprocessing import PolynomialFeatures

    # 多项式转换
    # 参数degree为度，默认值为2 ,度为2的多项式
    #eg:数据项：x1,x2转换为0次项，1次项和2次项，包含（1,x1,x2,x1*x1,x2*x2,x1*x2）
    return PolynomialFeatures().fit_transform(data)

def data_transform2(data, trans_fun=log1p):
    """
    基于单变元函数的数据变换可以使用一个统一的方式完成，
    使用preproccessing库的FunctionTransformer对数据进行变换
    FunctionTransformer 参数 inverse_func 为具体的函数

    :param data:
    :param trans_fun:
    :return:
    """
    from sklearn.preprocessing import FunctionTransformer

    # 自定义转换函数为对数函数的数据变换  log1p
    # 第一个参数是单变元函数
    # def inner1(data1):
    #     return data1*data1
    # FunctionTransformer(inner1).fit_transform(data)
    return FunctionTransformer(trans_fun).fit_transform(data)

if __name__ == '__main__':
    #1.加载数据
    data, target = loadData()

    #2.数据无量纲处理
    ## 2.1. 数据标准化处理
    standard_data = standardScaler(data)
    # ## 2.2.1 归一化： min-max
    # minMaxScaler_data = minMaxScaler(data)
    # ##2.2.2 归一化：l2归一化 normlizer
    # normlizer_data = normlizer(data)

    #3.定量特征二值化处理
    x = np.reshape(data[:, 1], (-1, 1))
    x_min = min(x)
    x_max = max(x)
    x_binarizer = binarizer(x, (x_max+x_min)/2)

    #4. 对定性特征进行编码处理
    ## 4.1 类别型输出变量进行编码处理
    Y = [['a', 'b'], ['b', 'x'], ['q', 'a']]
    encoded_Y = labelEncoder(Y)

    ## 4.2 输入变量为分类变量,独热编码
    X = [['a', 'b'], ['b', 'x'], ['q', 'a']]
    encoded_x = onehotEncoder(X)

    #5.缺失值计算
    data2 = np.vstack((np.array([nan, nan, nan, nan]), data))
    data2 = pd.DataFrame(data2)
    fill_data = missingValuesFill(data, fill_type='median')

    #6. 数据变换 trans_fun可自定义函数
    data_trans = data_transform2(data, trans_fun=log1p)



