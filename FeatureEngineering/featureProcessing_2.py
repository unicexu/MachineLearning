"""
特征工程 --- 2 特征选择

当数据预处理完成后，我们需要选择有意义的特征输入机器学习的算法和模型进行训练。
通常来说，从两个方面考虑来选择特征：
    1）特征是否发散：如果一个特征不发散，例如方差接近于0，也就是说样本在这个特征上基本上没有差异，这个特征对于样本的区分并没有什么用。
    2）特征与目标的相关性：这点比较显见，与目标相关性高的特征，应当优选选择。
    除方差法外，本文介绍的其他方法均从相关性考虑。

根据特征选择的形式又可以将特征选择方法分为3种：
    Filter：过滤法，按照发散性或者相关性对各个特征进行评分，设定阈值或者待选择阈值的个数，选择特征。
    Wrapper：包装法，根据目标函数（通常是预测效果评分），每次选择若干特征，或者排除若干特征。
    Embedded：集成法，先使用某些机器学习的算法和模型进行训练，得到各个特征的权值系数，根据系数从大到小选择特征。
              类似于Filter方法，但是是通过训练来确定特征的优劣。　
            　
使用sklearn中的feature_selection库来进行特征选择
"""
import numpy as np

def loadData():
    from sklearn.datasets import load_iris
    #导入数据集
    iris = load_iris()
    #特征矩阵
    data = iris.data
    #目标变量
    target = iris.target
    return data, target
#7. 特征选择
##7.1 Filter：过滤法
##            按照发散性或者相关性对各个特征进行评分，设定阈值或者待选择阈值的个数，选择特征。

###7.1.1 方差选择法 F检验
def filter_var(data, dat_threshold):
    """
    使用方差选择法，先要计算各个特征的方差，然后根据阈值，选择方差大于阈值的特征
    适用范围：特征取值连续，有监督，分类和回归
    """

    from sklearn.feature_selection import VarianceThreshold

    # 方差选择法，返回值为特征选择后的数据
    # 参数threshold为方差的阈值
    data_var = [np.var(data[i]) for i in range(data.shape[1])]
    print('特征方差：', data_var)
    return VarianceThreshold(threshold=dat_threshold).fit_transform(data)

###7.1.2 相关系数法 （自变量和因变量的显著性检验，数据类型为定量型数据)
def filter_pearsonr(data, target, topK=2):
    """
    使用相关系数法，先要计算各个特征对目标值的相关系数以及相关系数的P值

    皮尔逊相关系数( Pearson correlation coefficient），cov(x,y)[0,1]/(np.std(x)*np.std(y))
    又称皮尔逊积矩相关系数（Pearson product-moment correlation coefficient，简称 PPMCC或PCCs），
    是用于度量两个变量X和Y之间的相关（线性相关），其值介于-1与1之间,  两个变量之间的皮尔逊相关系数定义为两个变量之间的协方差和标准差的商

    :param data: 特征数据,数组，行代表样本数，列代表特征数
    :param target: 目标值
    :return:
    """
    from numpy import var, cov
    from sklearn.feature_selection import SelectKBest
    from scipy.stats import pearsonr#返回值 r, p-value ：r是相关系数，取值-1~1. p-value表示线性相关程度 p-value越小，表示相关程度越显著。

    # 选择K个最好的特征，返回选择特征后的数据
    # 第一个参数为计算评估特征是否好的函数，该函数输入特征矩阵和目标向量，输出二元组（评分，P值）的数组，数组第i项为第i个特征的评分和P值。在此定义为计算相关系数
    # 参数k为选择的特征个数

    pearsonr_xy = [np.cov(data[:, i], target)[0,1]/(np.std(data[:, i])*np.std(target))for i in range(data.shape[1])]
    print('特征的皮尔逊相关系数为：')
    print(pearsonr_xy)
    selectK = SelectKBest(lambda X, Y: tuple(map(tuple, np.array(list(map(lambda x: pearsonr(x, Y), X.T))).T)), k=topK).\
        fit_transform(data, target)
    return selectK

###7.1.3 卡方独立性检验（自变量和因变量的相关性检验：定性自变量和定性因变量的检验）
def filter_chi2(data, target, topK=2):
    """
    经典的卡方检验是检验定性自变量对定性因变量的相关性。
    假设自变量有N种取值，因变量有M种取值，考虑自变量等于i且因变量等于j的样本频数的观察值与期望的差距

    chi2(x,y)
    #H0假设：x,y相互独立
    返回两个结果：
        chi2 : array, shape = (n_features,)
               chi2 statistics of each feature.卡方统计量
        pval : array, shape = (n_features,)
               p-values of each feature.若选取显著性水平为0.05则，p<0.05，x，y不相互独立，两者存在某种关系；
               否则，不能拒绝原假设，尚不能代表x,y没有差别
    :param data:
    :param target:
    :param topK:选取相关性较强的前多少个特征
    :return:
    """
    from sklearn.feature_selection import SelectKBest
    from sklearn.feature_selection import chi2

    # 选择K个最好的特征，返回选择特征后的数据
    xy_chi2 = chi2(data, target)
    print('卡方统计量：', xy_chi2[0])
    print('显著性水平p，越小越好：', xy_chi2[1])
    return SelectKBest(chi2, k=topK).fit_transform(data, target)


###7.1.4  互信息法（经典的互信息也是评价定性自变量对定性因变量的相关性的）
def filter_mi(data, target, topK):
    """
    MutualInformation:简称MI
    互信息法（经典的互信息也是评价定性自变量对定性因变量的相关性的）,互信息越大，相关性越强
    :param data:
    :param target:
    :param topK:
    :return:
    """
    from sklearn.feature_selection import SelectKBest
    from minepy import MINE
    # 由于MINE的设计不是函数式的，定义mic方法将其为函数式的，返回一个二元组，二元组的第2项设置成固定的P值0.5
    def mic(x, y):
        m = MINE()
        m.compute_score(x, y)
        return (m.mic(), 0.5)

    mi_cal = lambda X, Y: tuple(map(tuple, np.array(list(map(lambda x: mic(x, Y), X.T))).T))#计算互信息
    print("互信息结果：", mi_cal(data, target))
    # 选择K个最好的特征，返回特征选择后的数据
    selectK = SelectKBest(mi_cal, k=topK).fit_transform(data, target)
    return selectK

##7.2 Wrapper：包装法，
###7.2.1 递归特征消除法
def wrapper_rfe(data, target, nums):
    """
    递归消除特征法使用一个基模型来进行多轮训练，每轮训练后，消除若干权值系数的特征，再基于新的特征集进行下一轮训练
    使用feature_selection库的RFE类来选择特征

    :param data: 
    :param target: 
    :param nums: 筛选的特征个数
    :return: 
    """
    from sklearn.feature_selection import RFE
    from sklearn.linear_model import LogisticRegression

    #递归特征消除法，返回特征选择后的数据
    #参数estimator为基模型
    #参数n_features_to_select为选择的特征个数
    features = RFE(estimator=LogisticRegression(solver='liblinear', multi_class='auto'), n_features_to_select=nums).fit_transform(data, target)
    return features

##7.3 Embedded：集成法，
###7.3.1 基于惩罚项的特征选择法
def embedded_l1_penalty(data, target):
    """
    使用带惩罚项的基模型，除了筛选出特征外，同时也进行了降维。
    使用feature_selection库的SelectFromModel类结合带L1惩罚项的逻辑回归模型，来选择特征

    :param data:
    :param target: 
    :return: 
    """
    from sklearn.feature_selection import SelectFromModel
    from sklearn.linear_model import LogisticRegression

    # 带L1惩罚项的逻辑回归作为基模型的特征选择
    features = SelectFromModel(LogisticRegression(solver='liblinear',multi_class='auto', penalty="l1", C=0.1)).fit_transform(data, target)

from sklearn.linear_model import LogisticRegression
class LR(LogisticRegression):
    def __init__(self, threshold=0.01, dual=False, tol=1e-4, C=1.0,
                 fit_intercept=True, intercept_scaling=1, class_weight=None,
                 random_state=None, solver='liblinear', max_iter=100,
                 multi_class='ovr', verbose=0, warm_start=False, n_jobs=1):

        #权值相近的阈值
        self.threshold = threshold
        LogisticRegression.__init__(self, penalty='l1', dual=dual, tol=tol, C=C,
                 fit_intercept=fit_intercept, intercept_scaling=intercept_scaling, class_weight=class_weight,
                 random_state=random_state, solver=solver, max_iter=max_iter,
                 multi_class=multi_class, verbose=verbose, warm_start=warm_start, n_jobs=n_jobs)
        #使用同样的参数创建L2逻辑回归
        self.l2 = LogisticRegression(penalty='l2', dual=dual, tol=tol, C=C, fit_intercept=fit_intercept, intercept_scaling=intercept_scaling, class_weight = class_weight, random_state=random_state, solver=solver, max_iter=max_iter, multi_class=multi_class, verbose=verbose, warm_start=warm_start, n_jobs=n_jobs)

    def fit(self, X, y, sample_weight=None):
        #训练L1逻辑回归
        super(LR, self).fit(X, y, sample_weight=sample_weight)
        self.coef_old_ = self.coef_.copy()
        #训练L2逻辑回归
        self.l2.fit(X, y, sample_weight=sample_weight)

        cntOfRow, cntOfCol = self.coef_.shape
        #权值系数矩阵的行数对应目标值的种类数目
        for i in range(cntOfRow):
            for j in range(cntOfCol):
                coef = self.coef_[i][j]
                #L1逻辑回归的权值系数不为0
                if coef != 0:
                    idx = [j]
                    #对应在L2逻辑回归中的权值系数
                    coef1 = self.l2.coef_[i][j]
                    for k in range(cntOfCol):
                        coef2 = self.l2.coef_[i][k]
                        #在L2逻辑回归中，权值系数之差小于设定的阈值，且在L1中对应的权值为0
                        if abs(coef1-coef2) < self.threshold and j != k and self.coef_[i][k] == 0:
                            idx.append(k)
                    #计算这一类特征的权值系数均值
                    mean = coef / len(idx)
                    self.coef_[i][idx] = mean
        return self
def embedded_l1_l2(data, target):
    """
     实际上，L1惩罚项降维的原理在于保留多个对目标值具有同等相关性的特征中的一个，所以没选到的特征不代表不重要。
     故，可结合L2惩罚项来优化。
     具体操作为：若一个特征在L1中的权值为1，选择在L2中权值差别不大且在L1中权值为0的特征构成同类集合，
     将这一集合中的特征平分L1中的权值，故需要构建一个新的逻辑回归模型

    :param data:
    :param target:
    :return:
    """
    from sklearn.feature_selection import SelectFromModel

    # 带L1和L2惩罚项的逻辑回归作为基模型的特征选择
    # 参数threshold为权值系数之差的阈值
    features = SelectFromModel(LR(threshold=0.5, C=0.1)).fit_transform(data, target)
    return features

###7.3.1 基于树模型的特征选择法
def embedded_tree_based(data, target):
    """
    树模型中GBDT,etc,dt 也可用来作为基模型进行特征选择，
    使用feature_selection库的SelectFromModel类结合GBDT模型
    :param data:
    :param target:
    :return:
    """
    from sklearn.ensemble import ExtraTreesClassifier
    from sklearn.ensemble import GradientBoostingClassifier
    from sklearn.feature_selection import SelectFromModel
    from sklearn import tree
    from matplotlib import pyplot as plt

    ### dt 决策树
    # clf = tree.DecisionTreeClassifier()
    ## etc
    # clf = ExtraTreesClassifier(n_estimators=50)
    ### gbdt
    clf = GradientBoostingClassifier()

    clf = clf.fit(data, target)
    importances = clf.feature_importances_
    print('the Gini importance of the features: ', importances)

    model = SelectFromModel(clf, prefit=True)
    data_new = model.transform(data)

    indices = np.argsort(importances)[::-1]#重要性降序排序
    #特征重要性绘图
    plt.figure()
    plt.title("Feature importances")
    plt.bar(range(data.shape[1]), importances[indices],
            color="r", align="center")
    plt.xticks(range(data.shape[1]), indices)
    plt.xlim([-1, data.shape[1]])
    plt.show()

    return data_new

if __name__ == '__main__':
    data, target = loadData()
    selectKBest = filter_pearsonr(data, target, 3)
    select_chi2 = filter_chi2(data, target)

    select_mi = filter_mi(data, target, 2)
    print("原始特征第一行：", data[0,:])
    print('筛选特征第一行：', select_mi[0, :])
    select_rfe = wrapper_rfe(data, target, 3)
    select_rfe[0, :]

    data_new = embedded_tree_based(data, target)