"""
奇异点检测：训练数据中没有离群点，我们是对检测新发现的样本点感兴趣；
异常点检测：训练数据中包含离群点，我们需要适配训练数据中的中心部分（密集的部分），忽视异常点；
异常点监测和奇异值检测有着相同的目的，都是为了把大部分数据聚集的簇与少量的噪声样本点分开。
不同的是，对于异常点监测来说，我们没有一个干净的训练集（训练集中也有噪声样本）。
"""
print(__doc__)

def svm_demo():
    """
    =========================================
    SVM: Maximum margin separating hyperplane
    =========================================

    Plot the maximum margin separating hyperplane within a two-class
    separable dataset using a Support Vector Machine classifier with
    linear kernel.

    原文：https: // blog.csdn.net / qq_33039859 / article / details / 6981078
    绘制分割超平面
    """
    import numpy as np
    import matplotlib.pyplot as plt
    from sklearn import svm

    # we create 40 separable points
    np.random.seed(0)

    X = np.array([[3, 3], [4, 3], [1, 1]])
    Y = np.array([1, 1, -1])

    # fit the model
    clf = svm.SVC(kernel='linear')
    clf.fit(X, Y)

    # get the separating hyperplane
    w = clf.coef_[0]
    a = -w[0] / w[1]
    xx = np.linspace(-5, 5)
    yy = a * xx - (clf.intercept_[0]) / w[1]

    # plot the parallels to the separating hyperplane that pass through the
    # support vectors
    b = clf.support_vectors_[0]
    yy_down = a * xx + (b[1] - a * b[0])
    b = clf.support_vectors_[-1]
    yy_up = a * xx + (b[1] - a * b[0])

    # plot the line, the points, and the nearest vectors to the plane
    plt.plot(xx, yy, 'k-')
    plt.plot(xx, yy_down, 'k--')
    plt.plot(xx, yy_up, 'k--')

    plt.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1],
                s=80, facecolors='none')
    plt.scatter(X[:, 0], X[:, 1], c=Y, cmap=plt.cm.Paired)

    plt.axis('tight')
    plt.show()

    print(clf.decision_function(X))#点到分割超平面的距离


def oneClassSVM():
    """
        奇异值检测：

        原文：https: // blog.csdn.net / hustqb / article / details / 75216241
        https://blog.csdn.net/YE1215172385/article/details/79750703

        oneClassSVM：一类SVM
        outlier detection和novelty detection这两个概念。
        对于前者即异常检测，训练样本中含有异常样本，根据需要通过阈值nu来设定异常比例；
        而对于后者，训练样本中一般不含有异常样本，即通过训练正常样本后，模型用来发现
        测试集中的新颖点（也可以认为是异常点）。我这里讲的主要是后者novelty detection，
        所以nu也设定的比较小，通过画X_outliers这些新颖点（异常点），主要是想清楚的显示训练
        正常样本（不含异常点）后的模型（图中红色的圈），可以用来发现新颖点。

    :return:
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib.font_manager
    from sklearn import svm
    xx, yy = np.meshgrid(np.linspace(-5, 5, 500), np.linspace(-5, 5, 500))

    #  Generate train data
    X = 0.3 * np.random.randn(100, 2)
    X_train  = np.r_[X+2, X-2] # 全是正常的，预测值应为1

    #  Generate some regular novel observations
    X = 0.3 * np.random.randn(20, 2)
    X_test = np.r_[X+2, X-2] # 全是正常的，预测值应为1
    #  Generate some abnormal novel observations 产生异常点
    X_outliers = np.random.uniform(low=-4, high=4, size=(20, 2))# 全是异常的，预测值应为-1

    #训练模型 
    clf = svm.OneClassSVM(nu=0.0001, kernel='rbf', gamma=0.1)#nu=0.1表示训练集里有10%的叛徒
                                                            #SVM受参数nu的限制，不知道对每个数据集把nu设为多少合适
    clf.fit(X_train)
    y_pred_train = clf.predict(X_train) # 训练集的标签
    y_pred_test = clf.predict(X_test)  # 正常测试集的标签
    y_pred_outlier = clf.predict(X_outliers) # 异常测试集的标签
    n_error_train = y_pred_train[y_pred_train == -1].size
    n_error_test = y_pred_test[y_pred_test == -1].size
    n_error_outliers = y_pred_outlier[y_pred_outlier == -1].size

    #  plot the line, the points, and the nearest vectors to the plane
    Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])#计算样本点到分割超平面的函数距离
    Z = Z.reshape(xx.shape)

    plt.title("Novelty Detection")
    plt.contourf(xx, yy, Z, levels=np.linspace(Z.min(), 0, 7), cmap=plt.cm.PuBu)
    a = plt.contour(xx, yy, Z, levels=[0], linewidths=2, colors='darkred')
    plt.contourf(xx, yy, Z, levels=[0, Z.max()], colors='palevioletred')

    s = 40
    b1 = plt.scatter(X_train[:, 0], X_train[:, 1], c='black', s=s, edgecolors='k')
    b2 = plt.scatter(X_test[:, 0], X_test[:, 1], c='gold', s=s,
                     edgecolors='k')
    c = plt.scatter(X_outliers[:, 0], X_outliers[:, 1], c='white', s=s,
                    edgecolors='k')
    plt.axis('tight')
    plt.xlim((-5, 5))
    plt.ylim((-5, 5))
    plt.legend([a.collections[0], b1, b2, c],
               ["learned frontier", "training observations",
                "new regular observations", "new abnormal observations"],
               loc="upper left",
               prop=matplotlib.font_manager.FontProperties(size=11))
    plt.xlabel(
        "error train: %d/200 ; errors novel regular: %d/40 ; "
        "errors novel abnormal: %d/40"
        % (n_error_train, n_error_test, n_error_outliers))
    plt.show()

    """
    SVM是用一个超平面来区分类别一和类别二；
    而One-class SVM是用无数个超平面区分类别一和类别二三四...。
    也就是说用于区分本类和异类的边界是有无数超平面组成的。
    既然这样，这个方法中就应该有参数来调节超平面的选取，
    使它通融一下，放几个人进来（扩大边界）。
    
    严格来说，一分类的SVM并不是一个异常点监测算法，而是一个奇异点检测算法：
    它的训练集不能包含异常样本，否则的话，可能在训练时影响边界的选取。
    但是，对于高维空间中的样本数据集，如果它们做不出有关分布特点的假设，
    One-class SVM将是一大利器。
    """

#异常点检测


#1. EllipticEnvelope（高斯分布的协方差估计）
# 一个一般的思路是，假设常规数据隐含这一个已知的概率分布。
# 基于这个假设，我们尝试确定数据的形状（边界），也可以将远离边界的样本点定义为异常点。
# SKlearn提供了一个covariance.EllipticEnvelope类，它可以根据数据做一个鲁棒的协方差估计，
# 然后学习到一个包围中心样本点并忽视离群点的椭圆
#举个例子，假设合群数据都是高斯分布的，那么EllipticEnvelope会鲁棒的估计合群数据的位置和协方差，
# 而不会受到离群数据的影响。从该估计得到的马氏距离用于得出偏离度度量
def ellipticEnvelope_detection():
    #高斯分布的协方差估计
    import numpy as np
    import matplotlib.pyplot as plt

    from sklearn.covariance import EmpiricalCovariance, MinCovDet

    n_samples = 125 #训练集个数
    n_outliers = 25#异常点个数
    n_features = 2#特征数，二维的便于展示

    # generate data
    gen_cov = np.eye(n_features)#生成2*2单位阵
    gen_cov[0, 0] = 2.#修改矩阵元素
    X = np.dot(np.random.randn(n_samples, n_features), gen_cov) #矩阵相乘
    # add some outliers
    outliers_cov = np.eye(n_features)
    outliers_cov[np.arange(1, n_features), np.arange(1, n_features)] = 7.
    X[-n_outliers:] = np.dot(np.random.randn(n_outliers, n_features), outliers_cov)#125个训练集中后25变成异常数据
    #最后25个数据被替换为 np.dot(np.random.randn(n_outliers, n_features), outliers_cov)#
    # fit a Minimum Covariance Determinant (MCD) robust estimator to data
    robust_cov = MinCovDet().fit(X)#最小协方差确定

    # compare estimators learnt from the full data set with true parameters
    emp_cov = EmpiricalCovariance().fit(X)#最大协方差

    # #############################################################################
    # Display results
    fig = plt.figure()
    plt.subplots_adjust(hspace=-.1, wspace=.4, top=.95, bottom=.05)

    # Show data set
    subfig1 = plt.subplot(3, 1, 1)
    inlier_plot = subfig1.scatter(X[:, 0], X[:, 1],
                                  color='black', label='inliers')
    outlier_plot = subfig1.scatter(X[:, 0][-n_outliers:], X[:, 1][-n_outliers:],
                                   color='red', label='outliers')
    subfig1.set_xlim(subfig1.get_xlim()[0], 11.)
    subfig1.set_title("Mahalanobis distances of a contaminated data set:")

    # Show contours of the distance functions
    xx, yy = np.meshgrid(np.linspace(plt.xlim()[0], plt.xlim()[1], 100),
                         np.linspace(plt.ylim()[0], plt.ylim()[1], 100))
    zz = np.c_[xx.ravel(), yy.ravel()]

    mahal_emp_cov = emp_cov.mahalanobis(zz)
    mahal_emp_cov = mahal_emp_cov.reshape(xx.shape)
    emp_cov_contour = subfig1.contour(xx, yy, np.sqrt(mahal_emp_cov),
                                      cmap=plt.cm.PuBu_r,
                                      linestyles='dashed')

    mahal_robust_cov = robust_cov.mahalanobis(zz)
    mahal_robust_cov = mahal_robust_cov.reshape(xx.shape)
    robust_contour = subfig1.contour(xx, yy, np.sqrt(mahal_robust_cov),
                                     cmap=plt.cm.YlOrBr_r, linestyles='dotted')

    subfig1.legend([emp_cov_contour.collections[1], robust_contour.collections[1],
                    inlier_plot, outlier_plot],
                   ['MLE dist', 'robust dist', 'inliers', 'outliers'],
                   loc="upper right", borderaxespad=0)
    plt.xticks(())
    plt.yticks(())

    # Plot the scores for each point
    emp_mahal = emp_cov.mahalanobis(X - np.mean(X, 0)) ** (0.33)
    subfig2 = plt.subplot(2, 2, 3)
    subfig2.boxplot([emp_mahal[:-n_outliers], emp_mahal[-n_outliers:]], widths=.25)
    subfig2.plot(np.full(n_samples - n_outliers, 1.26),
                 emp_mahal[:-n_outliers], '+k', markeredgewidth=1)
    subfig2.plot(np.full(n_outliers, 2.26),
                 emp_mahal[-n_outliers:], '+k', markeredgewidth=1)
    subfig2.axes.set_xticklabels(('inliers', 'outliers'), size=15)
    subfig2.set_ylabel(r"$\sqrt[3]{\rm{(Mahal. dist.)}}$", size=16)
    subfig2.set_title("1. from non-robust estimates\n(Maximum Likelihood)")
    plt.yticks(())

    robust_mahal = robust_cov.mahalanobis(X - robust_cov.location_) ** (0.33)
    #计算给定观测样本点的马氏距离的平方
    # Parameters：观测点假定和之前训练用的数据集同分布
    # Returns：观测点的马氏距离的平方
    subfig3 = plt.subplot(2, 2, 4)
    subfig3.boxplot([robust_mahal[:-n_outliers], robust_mahal[-n_outliers:]],
                    widths=.25)
    subfig3.plot(np.full(n_samples - n_outliers, 1.26),
                 robust_mahal[:-n_outliers], '+k', markeredgewidth=1)
    subfig3.plot(np.full(n_outliers, 2.26),
                 robust_mahal[-n_outliers:], '+k', markeredgewidth=1)
    subfig3.axes.set_xticklabels(('inliers', 'outliers'), size=15)
    subfig3.set_ylabel(r"$\sqrt[3]{\rm{(Mahal. dist.)}}$", size=16)
    subfig3.set_title("2. from robust estimates\n(Minimum Covariance Determinant)")
    plt.yticks(())

    plt.show()

#2. 孤立森林检测异常值
def isolationForest_detection():
    """
    孤立森林是一个高效的异常点监测算法。
    SKLEARN提供了ensemble.IsolationForest模块。
    该模块在进行检测时，会随机选取一个特征，然后在所选特征的最大值和最小值随机选择一个分切面。
    该算法下整个训练集的训练就像一颗树一样，递归的划分。划分的次数等于根节点到叶子节点的路径距离d。
    所有随机树（为了增强鲁棒性，会随机选取很多树形成森林）的d的平均值，就是我们检测函数的最终结果。
    那些路径d比较小的，都是因为距离主要的样本点分布中心比较远的。
    也就是说可以通过寻找最短路径的叶子节点来寻找异常点。

    https://blog.csdn.net/extremebingo/article/details/80108247

    :return:
    """
    import numpy as np
    import matplotlib.pyplot as plt
    from sklearn.ensemble import IsolationForest

    rng = np.random.RandomState(42)

    # Generate train data
    X = 0.3 * rng.randn(100, 2)
    X_train = np.r_[X + 2, X - 2]
    # Generate some regular novel observations
    X = 0.3 * rng.randn(20, 2)
    X_test = np.r_[X + 2, X - 2]
    # Generate some abnormal novel observations
    X_outliers = rng.uniform(low=-4, high=4, size=(20, 2))

    # fit the model
    clf = IsolationForest(behaviour='new', max_samples=100,
                          random_state=rng, contamination='auto')
    clf.fit(X_train)
    y_pred_train = clf.predict(X_train)
    y_pred_test = clf.predict(X_test)
    y_pred_outliers = clf.predict(X_outliers)

    # plot the line, the samples, and the nearest vectors to the plane
    xx, yy = np.meshgrid(np.linspace(-5, 5, 50), np.linspace(-5, 5, 50))
    Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    plt.title("IsolationForest")
    plt.contourf(xx, yy, Z, cmap=plt.cm.Blues_r)

    b1 = plt.scatter(X_train[:, 0], X_train[:, 1], c='white',
                     s=20, edgecolor='k')
    b2 = plt.scatter(X_test[:, 0], X_test[:, 1], c='green',
                     s=20, edgecolor='k')
    c = plt.scatter(X_outliers[:, 0], X_outliers[:, 1], c='red',
                    s=20, edgecolor='k')
    plt.axis('tight')
    plt.xlim((-5, 5))
    plt.ylim((-5, 5))
    plt.legend([b1, b2, c],
               ["training observations",
                "new regular observations", "new abnormal observations"],
               loc="upper left")
    plt.show()


#3.LOF局部异常系数
"""
lof
"""
from __future__ import division
import warnings

def distance_euclidean(instance1, instance2):
    """Computes the distance between two instances. Instances should be tuples of equal length.
    Returns: Euclidean distance
    Signature: ((attr_1_1, attr_1_2, ...), (attr_2_1, attr_2_2, ...)) -> float"""
    def detect_value_type(attribute):
        """Detects the value type (number or non-number).
        Returns: (value type, value casted as detected type)
        Signature: value -> (str or float type, str or float value)"""
        from numbers import Number
        attribute_type = None
        if isinstance(attribute, Number):
            attribute_type = float
            attribute = float(attribute)
        else:
            attribute_type = str
            attribute = str(attribute)
        return attribute_type, attribute
    # check if instances are of same length
    if len(instance1) != len(instance2):
        raise AttributeError("Instances have different number of arguments.")
    # init differences vector
    differences = [0] * len(instance1)
    # compute difference for each attribute and store it to differences vector
    for i, (attr1, attr2) in enumerate(zip(instance1, instance2)):
        type1, attr1 = detect_value_type(attr1)
        type2, attr2 = detect_value_type(attr2)
        # raise error is attributes are not of same data type.
        if type1 != type2:
            raise AttributeError("Instances have different data types.")
        if type1 is float:
            # compute difference for float
            differences[i] = attr1 - attr2
        else:
            # compute difference for string
            if attr1 == attr2:
                differences[i] = 0
            else:
                differences[i] = 1
    # compute RMSE (root mean squared error)
    rmse = (sum(map(lambda x: x**2, differences)) / len(differences))**0.5
    return rmse

class LOF:
    """Helper class for performing LOF computations and instances normalization."""
    def __init__(self, instances, normalize=True, distance_function=distance_euclidean):
        self.instances = instances
        self.normalize = normalize
        self.distance_function = distance_function
        if normalize:
            self.normalize_instances()

    def compute_instance_attribute_bounds(self):
        min_values = [float("inf")] * len(self.instances[0]) #n.ones(len(self.instances[0])) * n.inf
        max_values = [float("-inf")] * len(self.instances[0]) #n.ones(len(self.instances[0])) * -1 * n.inf
        for instance in self.instances:
            min_values = tuple(map(lambda x,y: min(x,y), min_values,instance)) #n.minimum(min_values, instance)
            max_values = tuple(map(lambda x,y: max(x,y), max_values,instance)) #n.maximum(max_values, instance)

        diff = [dim_max - dim_min for dim_max, dim_min in zip(max_values, min_values)]
        if not all(diff):
            problematic_dimensions = ", ".join(str(i+1) for i, v in enumerate(diff) if v == 0)
            warnings.warn("No data variation in dimensions: %s. You should check your data or disable normalization." % problematic_dimensions)

        self.max_attribute_values = max_values
        self.min_attribute_values = min_values

    def normalize_instances(self):
        """Normalizes the instances and stores the infromation for rescaling new instances."""
        if not hasattr(self, "max_attribute_values"):
            self.compute_instance_attribute_bounds()
        new_instances = []
        for instance in self.instances:
            new_instances.append(self.normalize_instance(instance)) # (instance - min_values) / (max_values - min_values)
        self.instances = new_instances

    def normalize_instance(self, instance):
        return tuple(map(lambda value,max,min: (value-min)/(max-min) if max-min > 0 else 0,
                         instance, self.max_attribute_values, self.min_attribute_values))

    def local_outlier_factor(self, min_pts, instance):
        """The (local) outlier factor of instance captures the degree to which we call instance an outlier.
        min_pts is a parameter that is specifying a minimum number of instances to consider for computing LOF value.
        Returns: local outlier factor
        Signature: (int, (attr1, attr2, ...), ((attr_1_1, ...),(attr_2_1, ...), ...)) -> float"""
        if self.normalize:
            instance = self.normalize_instance(instance)
        return local_outlier_factor(min_pts, instance, self.instances, distance_function=self.distance_function)

def k_distance(k, instance, instances, distance_function=distance_euclidean):
    #TODO: implement caching
    """Computes the k-distance of instance as defined in paper. It also gatheres the set of k-distance neighbours.
    Returns: (k-distance, k-distance neighbours)
    Signature: (int, (attr1, attr2, ...), ((attr_1_1, ...),(attr_2_1, ...), ...)) -> (float, ((attr_j_1, ...),(attr_k_1, ...), ...))"""
    distances = {}
    for instance2 in instances:
        distance_value = distance_function(instance, instance2)
        if distance_value in distances:
            distances[distance_value].append(instance2)
        else:
            distances[distance_value] = [instance2]
    distances = sorted(distances.items())
    neighbours = []
    [neighbours.extend(n[1]) for n in distances[:k]]
    k_distance_value = distances[k - 1][0] if len(distances) >= k else distances[-1][0]
    return k_distance_value, neighbours

def reachability_distance(k, instance1, instance2, instances, distance_function=distance_euclidean):
    """The reachability distance of instance1 with respect to instance2.
    Returns: reachability distance
    Signature: (int, (attr_1_1, ...),(attr_2_1, ...)) -> float"""
    (k_distance_value, neighbours) = k_distance(k, instance2, instances, distance_function=distance_function)
    return max([k_distance_value, distance_function(instance1, instance2)])

def local_reachability_density(min_pts, instance, instances, **kwargs):
    """Local reachability density of instance is the inverse of the average reachability
    distance based on the min_pts-nearest neighbors of instance.
    Returns: local reachability density
    Signature: (int, (attr1, attr2, ...), ((attr_1_1, ...),(attr_2_1, ...), ...)) -> float"""
    (k_distance_value, neighbours) = k_distance(min_pts, instance, instances, **kwargs)
    reachability_distances_array = [0]*len(neighbours) #n.zeros(len(neighbours))
    for i, neighbour in enumerate(neighbours):
        reachability_distances_array[i] = reachability_distance(min_pts, instance, neighbour, instances, **kwargs)
    if not any(reachability_distances_array):
        warnings.warn("Instance %s (could be normalized) is identical to all the neighbors. Setting local reachability density to inf." % repr(instance))
        return float("inf")
    else:
        return len(neighbours) / sum(reachability_distances_array)

def local_outlier_factor(min_pts, instance, instances, **kwargs):
    """The (local) outlier factor of instance captures the degree to which we call instance an outlier.
    min_pts is a parameter that is specifying a minimum number of instances to consider for computing LOF value.
    Returns: local outlier factor
    Signature: (int, (attr1, attr2, ...), ((attr_1_1, ...),(attr_2_1, ...), ...)) -> float"""
    (k_distance_value, neighbours) = k_distance(min_pts, instance, instances, **kwargs)
    instance_lrd = local_reachability_density(min_pts, instance, instances, **kwargs)
    lrd_ratios_array = [0]* len(neighbours)
    for i, neighbour in enumerate(neighbours):
        instances_without_instance = set(instances)
        instances_without_instance.discard(neighbour)
        neighbour_lrd = local_reachability_density(min_pts, neighbour, instances_without_instance, **kwargs)
        lrd_ratios_array[i] = neighbour_lrd / instance_lrd
    return sum(lrd_ratios_array) / len(neighbours)

def outliers(k, instances, **kwargs):
    """Simple procedure to identify outliers in the dataset."""
    instances_value_backup = instances
    outliers = []
    for i, instance in enumerate(instances_value_backup):
        instances = list(instances_value_backup)
        instances.remove(instance)
        l = LOF(instances, **kwargs)
        value = l.local_outlier_factor(k, instance)
        if value > 1:
            outliers.append({"lof": value, "instance": instance, "index": i})
    outliers.sort(key=lambda o: o["lof"], reverse=True)
    return outliers


def test_lof():
    print("Running tests, nothing more should appear if everything goes well.")

    instances = (
        (1, 1, 1, 1),
        (2, 2, 2, 2),
        (3, 3, 3, 3),
        (2, 1, 1, 1),
        (1, 2, 1, 1),
        (13, 13, 13, 13)
    )
    instance = (1, 1, 1, 2)

    #test LOF_normalize_instances():
    l = LOF(((1, 1), (2, 2)), normalize=True)
    assert l.instances == [(0.0, 0.0), (1.0, 1.0)]
    l = LOF(((1, 1), (2, 2), (3, 3)), normalize=True)
    assert l.instances == [(0.0, 0.0), (0.5, 0.5), (1.0, 1.0)]

    #test distance():
    assert 1 == distance_euclidean((1, 1), (2, 2))

    #test k_distance():
    instances = ((1, 1), (2, 2), (3, 3))
    d = k_distance(1, (2, 2), instances)
    assert d == (0.0, [(2, 2)])
    d = k_distance(1, (2.2, 2.2), instances)
    assert d == (0.20000000000000018, [(2, 2)])
    d = k_distance(1, (2.5, 2.5), instances)
    assert d == (0.5, [(2, 2), (3, 3)])
    d = k_distance(5, (2.2, 2.2), instances)
    assert d == (1.2000000000000002, [(2, 2), (3, 3), (1, 1)])

    # test reachability_distance():
    instances = ((1, 1), (2, 2), (3, 3))
    reachability_distance(1, (1, 1), (2, 2), instances)

    # test outliers():
    outliers(1, instances)

    # test normalization_problems():
    # see issue https://github.com/damjankuznar/pylof/issues/7
    instances = [(1., 2., 3.), (2., 3., 4.), (1., 2., 4.), (1., 2., 1.)]
    l = outliers(1, instances)

    # test_duplicate_instances():
    instances = (
            (1, 1, 1, 1),
            (2, 2, 2, 2),
            (3, 3, 3, 3),
            (2, 1, 1, 1),
            (1, 2, 1, 1),
            (2, 2, 2, 2),
            (2, 2, 2, 2),
            (1, 1, 1, 1),
            (1, 1, 1, 1),
            (13, 13, 13, 13)
        )
    outliers(1, instances)


#比较几种异常点检测方法
def compare_outliers_detection():
    # Author: Alexandre Gramfort <alexandre.gramfort@inria.fr>
    #         Albert Thomas <albert.thomas@telecom-paristech.fr>
    # License: BSD 3 clause

    import time

    import numpy as np
    import matplotlib
    import matplotlib.pyplot as plt

    from sklearn import svm
    from sklearn.datasets import make_moons, make_blobs
    from sklearn.covariance import EllipticEnvelope
    from sklearn.ensemble import IsolationForest
    from sklearn.neighbors import LocalOutlierFactor

    print(__doc__)

    matplotlib.rcParams['contour.negative_linestyle'] = 'solid'

    # Example settings
    n_samples = 300
    outliers_fraction = 0.15
    n_outliers = int(outliers_fraction * n_samples)
    n_inliers = n_samples - n_outliers

    # define outlier/anomaly detection methods to be compared
    anomaly_algorithms = [
        ("Robust covariance", EllipticEnvelope(contamination=outliers_fraction)),
        ("One-Class SVM", svm.OneClassSVM(nu=outliers_fraction, kernel="rbf",
                                          gamma=0.1)),
        ("Isolation Forest", IsolationForest(behaviour='new',
                                             contamination=outliers_fraction,
                                             random_state=42)),
        ("Local Outlier Factor", LocalOutlierFactor(
            n_neighbors=35, contamination=outliers_fraction))]

    # Define datasets
    blobs_params = dict(random_state=0, n_samples=n_inliers, n_features=2)
    datasets = [
        make_blobs(centers=[[0, 0], [0, 0]], cluster_std=0.5,
                   **blobs_params)[0],
        make_blobs(centers=[[2, 2], [-2, -2]], cluster_std=[0.5, 0.5],
                   **blobs_params)[0],
        make_blobs(centers=[[2, 2], [-2, -2]], cluster_std=[1.5, .3],
                   **blobs_params)[0],
        4. * (make_moons(n_samples=n_samples, noise=.05, random_state=0)[0] -
              np.array([0.5, 0.25])),
        14. * (np.random.RandomState(42).rand(n_samples, 2) - 0.5)]

    # Compare given classifiers under given settings
    xx, yy = np.meshgrid(np.linspace(-7, 7, 150),
                         np.linspace(-7, 7, 150))

    plt.figure(figsize=(len(anomaly_algorithms) * 2 + 3, 12.5))
    plt.subplots_adjust(left=.02, right=.98, bottom=.001, top=.96, wspace=.05,
                        hspace=.01)

    plot_num = 1
    rng = np.random.RandomState(42)

    for i_dataset, X in enumerate(datasets):
        # Add outliers
        X = np.concatenate([X, rng.uniform(low=-6, high=6,
                                           size=(n_outliers, 2))], axis=0)

        for name, algorithm in anomaly_algorithms:
            t0 = time.time()
            algorithm.fit(X)
            t1 = time.time()
            plt.subplot(len(datasets), len(anomaly_algorithms), plot_num)
            if i_dataset == 0:
                plt.title(name, size=18)

            # fit the data and tag outliers
            if name == "Local Outlier Factor":
                y_pred = algorithm.fit_predict(X)
            else:
                y_pred = algorithm.fit(X).predict(X)

            # plot the levels lines and the points
            if name != "Local Outlier Factor":  # LOF does not implement predict
                Z = algorithm.predict(np.c_[xx.ravel(), yy.ravel()])
                Z = Z.reshape(xx.shape)
                plt.contour(xx, yy, Z, levels=[0], linewidths=2, colors='black')

            colors = np.array(['#377eb8', '#ff7f00'])
            plt.scatter(X[:, 0], X[:, 1], s=10, color=colors[(y_pred + 1) // 2])

            plt.xlim(-7, 7)
            plt.ylim(-7, 7)
            plt.xticks(())
            plt.yticks(())
            plt.text(.99, .01, ('%.2fs' % (t1 - t0)).lstrip('0'),
                     transform=plt.gca().transAxes, size=15,
                     horizontalalignment='right')
            plot_num += 1

    plt.show()